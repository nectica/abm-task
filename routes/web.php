<?php

use App\Http\Controllers\ContactController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MineralValorController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\StatusContactController;
use App\Http\Controllers\UserAbmController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Auth::routes();
Route::group(['middleware' => ['auth']], function() {

    Route::get('/', [ HomeController::class, 'index']);
    Route::get('/Home/event', [ HomeController::class, 'getEventsHome'])->name('home.event');
    Route::get('/Home/query', [ HomeController::class, 'getQueriesHome'])->name('home.query');
    Route::get('/Home/reports', [ HomeController::class, 'getReportsHome'])->name('home.report');
    Route::get('/Home/user', [ HomeController::class, 'getUsersHome'])->name('home.user');

    Route::resource('roles', RoleController::class);

    Route::resource('users', UserController::class);
    //news Routes
    Route::get('/news',[NewsController::class, 'index'])->name('news.index');
    Route::get('/news/create',[NewsController::class, 'create'])->name('news.create');
    Route::post('/news',[NewsController::class, 'store'])->name('news.store');
    Route::get('/news/{news}',[NewsController::class, 'show'])->name('news.show');
    Route::get('/news/{news}/edit',[NewsController::class, 'edit'])->name('news.edit');
    Route::put('/news',[NewsController::class, 'update'])->name('news.update');
    Route::delete('/news/{news}',[NewsController::class, 'destroy'])->name('news.delete');
    Route::post('/news/filter',[NewsController::class, 'filter'])->name('news.filter');
    //event Routes
    Route::resource('event', EventController::class,
    [
        'names' => [
            'index' => 'event.index',
            'create' => 'event.create',
            'store' => 'event.store',
            'show' => 'event.show',
            'edit' => 'event.edit',
            'update' => 'event.update',
            'destroy' => 'event.delete',
        ]
    ]
    );
    Route::post('/event/filter',[EventController::class, 'filter'])->name('event.filter');

    //UserAbm Routes
    Route::resource('UserAbm', UserAbmController::class,
    [
        'names' => [
            'index' => 'userAbm.index',
            'create' => 'userAbm.create',
            'store' => 'userAbm.store',
            'show' => 'userAbm.show',
            'edit' => 'userAbm.edit',
            'update' => 'userAbm.update',
            'destroy' => 'userAbm.delete',
        ]
    ]
    );
    Route::post('userAbm/filter', [UserAbmController::class, 'filter'])->name('userAbm.filter');

    //report Routes
    Route::resource('report', ReportController::class,
    [
        'names' => [
            'index' => 'report.index',
            'create' => 'report.create',
            'store' => 'report.store',
            'show' => 'report.show',
            'edit' => 'report.edit',
            'update' => 'report.update',
            'destroy' => 'report.delete',
        ]
    ]
    );
    Route::post('report/filter', [ReportController::class, 'filter'])->name('report.filter');
    Route::get('report/download/{report}', [ReportController::class, 'download'])->name('report.download');

    //
     //Contact Routes
     Route::resource('query', ContactController::class,
     [
         'names' => [
             'index' => 'query.index',
             'create' => 'query.create',
             'store' => 'query.store',
             'show' => 'query.show',
             'edit' => 'query.edit',
             'update' => 'query.update',
             'destroy' => 'query.delete',
         ]
     ]
     );
    Route::post('query/filter', [ContactController::class, 'filter'])->name('query.filter');
    //mineral
    Route::resource('mineral', MineralValorController::class, [
            'names' => [
                'index' => 'mineralesValor.index',
                'create' => 'mineralesValor.create',
                'store' => 'mineralesValor.store',
                'show' => 'mineralesValor.show',
                'edit' => 'mineralesValor.edit',
                'update' => 'mineralesValor.update',
                'destroy' => 'mineralesValor.delete',
            ]
        ]
    );
    Route::post('mineral/filter', [MineralValorController::class, 'filter'])->name('mineralesValor.filter');


});
Route::get('userAbm/recovery-password/{id}/{token}', [UserAbmController::class, 'recoveryPassword'])->name('userAbm.recoveryPassword');
Route::put('userAbm/recovery-password', [UserAbmController::class, 'updatePassword'])->name('userAbm.updatePassword');

//UserAbm Routes
Route::resource('status/contact', StatusContactController::class,
[
    'names' => [
        'index' => 'status.index',
        'create' => 'status.create',
        'store' => 'status.store',
        'show' => 'status.show',
        'edit' => 'status.edit',
        'update' => 'status.update',
        'destroy' => 'status.delete',
    ]
]
)->parameters(['contact'=> 'statusContact']);
