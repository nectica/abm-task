<?php

use App\Http\Controllers\Api\ContactApiController;
use App\Http\Controllers\Api\EventApiController;
use App\Http\Controllers\Api\LoginKeyController;
use App\Http\Controllers\Api\NewsApiContoller;
use App\Http\Controllers\Api\ReportApiController;
use App\Http\Controllers\Api\UserAbmApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth.abm')->get('login', [UserAbmApiController::class, 'login']);
Route::/* middleware('auth.abm')-> */get('report/{report}', [ReportApiController::class, 'getReport']);
Route::get('report/paginate/{from?}/{limited?}', [ReportApiController::class, 'getReportPaginate']);

Route::middleware('auth.abm')->prefix('userAbm')->group(function(){
    Route::get('/',[UserAbmApiController::class, 'index']);
    //Route::post('create', [UserAbmApiController::class, 'store']);
    Route::get('user-type/{executive}/{patner}',[UserAbmApiController::class, 'getUserByUserType']);
    Route::post('user',[UserAbmApiController::class, 'getUserByEmail']);
    Route::put('update/profile',[UserAbmApiController::class, 'update']);
    Route::put('update/password',[UserAbmApiController::class, 'updatePassword']);

});
Route::post('userAbm/create', [UserAbmApiController::class, 'store']);
Route::post('userAbm/recoveryPassword', [UserAbmApiController::class, 'recoveryPassword']);

Route::middleware('auth.abm')->prefix('/news')->group( function(){
    Route::get('/',[NewsApiContoller::class,'index']);
    Route::put('/update',[NewsApiContoller::class,'update']);
    Route::get('destacados', [NewsApiContoller::class,'getNewsDestacado']);
    Route::get('{id}',[NewsApiContoller::class,'show']);
    Route::get('user/{id}', [NewsApiContoller::class,'getNewsByUser']);
    Route::post('filter', [NewsApiContoller::class, 'filter']);
    Route::get('paginate/{from?}/{limited?}', [NewsApiContoller::class, 'getNewspaginate']);

});

Route::middleware('auth.abm')->prefix('event')->group(function(){
    Route::get('/', [EventApiController::class, 'index']);
    Route::get('/{id}', [EventApiController::class, 'show']);
    Route::get('/destacado', [EventApiController::class, 'getDestacados']);
    Route::post('filter', [EventApiController::class, 'filter']);

});
Route::middleware('auth.abm')->prefix('query')->group(function(){
    Route::post('create', [ContactApiController::class, 'store']);
});
Route::middleware('auth.abm')->prefix('key/user')->group(function(){
	Route::get('/{user}', [LoginKeyController::class, 'index']);
	Route::post('/', [LoginKeyController::class, 'create']);
});
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
