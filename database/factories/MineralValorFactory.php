<?php

namespace Database\Factories;

use App\Models\Mineral;
use App\Models\MineralValor;
use Illuminate\Database\Eloquent\Factories\Factory;

class MineralValorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MineralValor::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $minerales = Mineral::get('id');
        return [
            'ano' => '2020',
            'mes' => $this->faker->numberBetween(0, 11),
            'valor' => $this->faker->randomFloat(3, 50, 10000),
            'exportacion' => $this->faker->randomNumber(3),
            'mineral_id' => $minerales[$this->faker->numberBetween(0,( count($minerales) - 1 ) )]->id,
        ];
    }
}
