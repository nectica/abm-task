<?php

namespace Database\Factories;

use App\Models\Event;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\File;

class EventFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Event::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $filepath = storage_path('app/public/event');

        if(! File::exists($filepath)){
            File::makeDirectory($filepath);
        }
        return [
            'title' => $this->faker->sentence(),
            'details' => $this->faker->paragraph(6),
            'image' => $this->faker->image($filepath , 400,300, null, false),
            'eventDate' => $this->faker->dateTimeBetween('now', '+1 years'),
            'destacado' =>$this->faker->boolean(),
        ];
    }
}
