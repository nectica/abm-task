<?php

namespace Database\Factories;

use App\Models\UserAbm;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;

class UserAbmFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserAbm::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'password' => Hash::make('123456'),
            'phone' => $this->faker->phoneNumber(),
            'company' => $this->faker->company(),
            'jobTitle' => $this->faker->jobTitle(),
            'executive' => $this->faker->boolean(),
            'patner' => $this->faker->boolean(),
        ];
    }
}
