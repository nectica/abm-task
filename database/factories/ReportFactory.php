<?php

namespace Database\Factories;

use App\Models\Report;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\File;

class ReportFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Report::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(){
        $filepath = storage_path('app/public/reports');
        if(!File::exists($filepath)){
            File::makeDirectory($filepath);
        }
        return [
            'title' => $this->faker->sentence(),
            'copete' => $this->faker->sentence(),
            'fileName' => $this->faker->name(),
            'report' =>$this->faker->image($filepath, 640, 480, null, false ),
            'patner' => $this->faker->boolean(),
            'executive' => $this->faker->boolean(),
        ];
    }
}
