<?php

namespace Database\Factories;

use App\Models\News;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\File;

class NewsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = News::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $filepath = storage_path('app/public/news');

        if(! File::exists($filepath)){
            File::makeDirectory($filepath);
        }
        return [
            'title' => $this->faker->sentence(),
            'details' => $this->faker->paragraph(),
            'image' => $this->faker->image($filepath, 640, 480, null, false ),
            'executive' => $this->faker->boolean(),
            'patner' => $this->faker->boolean(),
            'destacado' =>$this->faker->boolean(),
        ];
    }
}
