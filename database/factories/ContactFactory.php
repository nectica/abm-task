<?php

namespace Database\Factories;

use App\Models\Contact;
use Faker\Provider\bg_BG\PhoneNumber;
use Illuminate\Database\Eloquent\Factories\Factory;

class ContactFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Contact::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name() ,
            'company' => $this->faker->company(),
            'phone' => $this->faker->PhoneNumber(),
            'topic' => $this->faker->sentence(),
            'query' => $this->faker->paragraph(6),
        ];
    }
}
