<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;


class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'news-list',
            'news-create',
            'news-edit',
            'news-delete',
            'event-list',
            'event-create',
            'event-edit',
            'event-delete',
            'report-list',
            'report-create',
            'report-edit',
            'report-delete',
            'mineral-list',
            'mineral-create',
            'mineral-edit',
            'mineral-delete',

         ];
         foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);

         }
    }
}
