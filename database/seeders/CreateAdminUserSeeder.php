<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([

            'name' => 'Administrador Del sistema.',

            'email' => 'admin@admin.com',

            'password' => bcrypt('123456')

        ]);
        $roleAdmin = Role::create(['name' => 'Admin']);
        $permissions = Permission::pluck('id','id')->all();

        $roleAdmin ->syncPermissions($permissions);

        $roleReader = Role::create(['name' => 'lector']);
        $permissions = Permission::where('name', 'news-list')->get();
        $roleReader->syncPermissions([$permissions]);

        $user->assignRole([$roleAdmin ->id]);
    }
}
