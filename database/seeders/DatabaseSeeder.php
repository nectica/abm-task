<?php

namespace Database\Seeders;

use App\Models\Contact;
use App\Models\Event;
use App\Models\Mineral;
use App\Models\MineralValor;
use App\Models\News;
use App\Models\Report;
use App\Models\UserAbm;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        News::factory(20)->create();
        Event::factory(20)->create();
        UserAbm::factory(20)->create();
        Report::factory(20)->create();
        Contact::factory(20)->create();
        Mineral::factory(5)->create();
        MineralValor::factory(25)->create();

    }
}
