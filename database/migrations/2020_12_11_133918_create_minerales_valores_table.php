<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMineralesValoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('minerales_valores', function (Blueprint $table) {
            $table->id();
            $table->integer('ano');
            $table->integer('mes');
            $table->double('valor', 12, 3);
            $table->bigInteger('exportacion');
            $table->bigInteger('mineral_id')->unsigned()->nullable();
            $table->timestamps();
        });
        Schema::table('minerales_valores', function (Blueprint $table) {
            $table->foreign('mineral_id')->references('id')->on('minerales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('minerales_valores');
    }
}
