<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTokenAndDueTokenFieldsToUsersAbmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_abm', function (Blueprint $table) {
            $table->string('token')->nullable()->after('password');
            $table->dateTime('expire_token')->nullable()->after('password');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_abm', function (Blueprint $table) {
            //
        });
    }
}
