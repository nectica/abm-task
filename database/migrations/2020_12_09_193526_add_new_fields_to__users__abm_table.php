<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFieldsToUsersAbmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_abm', function (Blueprint $table) {
            $table->string('password')->after('email');
            $table->string('phone')->after('email');
            $table->string('company')->after('email');
            $table->string('jobTitle')->after('email')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('_users__abm', function (Blueprint $table) {
            //
        });
    }
}
