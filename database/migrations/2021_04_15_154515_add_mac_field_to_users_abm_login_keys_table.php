<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMacFieldToUsersAbmLoginKeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_abm_login_keys', function (Blueprint $table) {
			$table->string('mac',500);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_abm_login_keys', function (Blueprint $table){
			$table->dropColumn('mac');
		});
    }
}
