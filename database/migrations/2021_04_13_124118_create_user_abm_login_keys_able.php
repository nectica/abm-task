<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserAbmLoginKeysAble extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_abm_login_keys', function (Blueprint $table) {
            $table->id();
			$table->unsignedBigInteger('userAbm_id');
			$table->string('key', 255);
            $table->timestamps();
        });
		Schema::table('user_abm_login_keys', function (Blueprint $table){
			$table->foreign('userAbm_id')->references('id')->on('users_abm');

		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_abm_login_keys');
    }
}
