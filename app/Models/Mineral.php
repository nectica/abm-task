<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mineral extends Model
{
    use HasFactory;
    protected $table = 'minerales';
/*     protected $with = ['Valores'];
 */    public function Valores(){
        return $this->hasMany(MineralValor::class)->groupBy('mineral_id','mes');
    }
}
