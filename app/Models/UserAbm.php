<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;


class UserAbm extends Model
{
    use HasFactory, Sortable;
    protected $table = 'users_abm';
    public $sortable = ['id',
    'name',
    'email',
    'patner',
    'executive'];
    public $fillable = [
        'name',
        'email',
        'patner',
        'executive',
        'password' ,
        'phone' ,
        'company',
        'jobTitle',
    ];


    public function contacts(){
        return $this->hasMany('App\Models\Contact', 'userAbm_id');
    }
	public function loginKeys(){
		return $this->hasMany(LoginKey::class, 'userAbm_id');
	}
}
