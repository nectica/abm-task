<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FirebaseResponse extends Model
{
    use HasFactory;
	protected $fillable = ['response'];
	protected $table = 'firebase_response';
}
