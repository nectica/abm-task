<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class News extends Model
{
    use HasFactory, Sortable;
    protected $table = 'news';
    public $sortable = [
        'title',
        'details',
        'image',
        'patner',
        'executive',
        'destacado',
    ];
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
