<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class MineralValor extends Model
{
    use HasFactory, Sortable;
    protected $table = 'minerales_valores';
    public $sortable = [
        'id',
        'ano',
        'mes',
        'valor',
        'exportacion',
        'mineral_id',
    ];
    public $fillable = [
        'ano',
        'mes',
        'valor',
        'exportacion',
        'mineral_id',
    ];
    /* protected $with = ['Mineral']; */
    public function Mineral(){
        return $this->belongsTo(Mineral::class, 'mineral_id');
    }

}
