<?php

namespace App\Models;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;


class Event extends Model
{
    use HasFactory, Sortable, SoftDeletes;
    public $sortable = [
        'title',
        'details',
        'image',
        'eventDate',
        'patner',
        'executive',
        'destacado',
    ];
    protected $hidden = ['deleted_at'];

}
