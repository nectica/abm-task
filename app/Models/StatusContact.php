<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatusContact extends Model
{
    use HasFactory;
    protected $table = 'status_contacts';
    protected $fillable = ['name'];

}
