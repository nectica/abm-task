<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoginKey extends Model
{
    use HasFactory;
	protected $table = 'user_abm_login_keys';
	protected $fillable = [ 'userAbm_id', 'key', 'mac','updated_at'];
	public function user(){
		return $this->belongsTo(UserAbm::class, 'userAbm_id');
	}
}
