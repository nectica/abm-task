<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Report extends Model
{
    use HasFactory, Sortable;
    public $sortable = [
        'title',
        'copete',
        'fileName',
        'report',
        'patner',
        'executive',
    ];
}
