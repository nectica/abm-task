<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;



class Contact extends Model
{
    use HasFactory, Sortable;
    public $sortable =
    [
        'name',
        'company',
        'phone',
        'topic',
        'query',
		'status_contacts_id',
        'created_at',
    ];
    protected $fillable = ['name',
        'company',
        'phone',
        'topic',
        'query',
        'userAbm_id',
        'status_contacts_id',
    ];
    public function userAbm()
    {
        return $this->belongsTo('App\Models\UserAbm', 'userAbm_id');
    }

	public function status(){
		return $this->belongsTo('App\Models\StatusContact','status_contacts_id');
	}

}
