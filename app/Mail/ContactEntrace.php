<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactEntrace extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $contact ;
    public function __construct($contact)
    {
        $this->contact = $contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $main = 'info@caem.com.ar';
        return $this->to($main)->cc($this->getCC())->view('mail.Contact')->subject('Consulta Caem');
    }
    public function getCC(){
        $cc = [];
        switch($this->contact->subject){
            case 'temas administrativos':
                array_push($cc, 'administracion@caem.com.ar');
            break;
			case 'comunicación y prensa':
				array_push($cc, 'comunicacion@caem.com.ar');
            break;
            case 'actividades CAEM':
                array_push($cc, ' comercial@caem.com.ar');
            break;
            case 'gremiales':
                array_push($cc, 'legales@caem.com.ar');
            break;
            case 'sustentabilidad':
                array_push($cc, 'sustentabilidad@caem.com.ar');
            break;
            case 'información sectorial':
                array_push($cc, 'economia@caem.com.ar');
                array_push($cc, 'industria@caem.com.ar');
            break;

            break;
        }
            return $cc;
    }
}

