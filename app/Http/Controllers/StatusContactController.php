<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreStatusContacts;
use App\Models\StatusContact;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class StatusContactController extends Controller
{
    public function index(){
        $estados = StatusContact::all();
        return view('status.index', compact('estados'));
    }
    public function create(){
        $users = User::all();
        return view('status.create', compact('users'));
    }
    public function store(StoreStatusContacts $request){
        StatusContact::create($request->all());
        return redirect()->route('status.index')->with('success', 'Se ha creado la estado correctamente');
    }
    public function edit(StatusContact $statusContact){
        $users = User::all();


        return view('status.update', compact('statusContact', 'users') );
    }

    public function update(StoreStatusContacts $request, StatusContact $statusContact){
        $statusContact->update($request->all());
        return redirect()->to($request->url)->with('success', 'Se ha Actualizado el estado correctamente');

    }

    public function destroy(StatusContact $statusContact ){
        $statusContact->delete();
        return redirect()->route('status.index')->with('success', 'Se ha borrado el estado correctamente');

    }
}
