<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Event;
use Illuminate\Http\Request;

class EventApiController extends Controller
{
    public function index(Request $request){

        $events = Event::where('destacado', 0 )
            ->orderBy('eventDate', 'desc')
            ->orderBy('created_at', 'desc')
            ->get();
        return ResponseApi::response($events);
    }
    public function show($id){
        $event = Event::find($id);
        return ResponseApi::response($event);
    }
    public function getDestacados(Request $request){
        $events = Event::where('destacado', 1 )
            ->orderBy('eventDate', 'desc')
            ->orderBy('created_at', 'desc')
            ->get();
        return ResponseApi::response($events);
    }
    public function filter(Request $request){
        $event = Event::where('title', 'like', '%'.$request->title.'%')
            ->orWhere('details', 'like', '%'.$request->title.'%')
            ->where('destacado', 0 )
            ->orderBy('eventDate', 'desc')
            ->get();
        return ResponseApi::response($event);
    }
}
