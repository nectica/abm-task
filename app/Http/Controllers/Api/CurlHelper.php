<?php
namespace App\Http\Controllers\Api;

use App\Models\FirebaseGroup;
use App\Models\FirebaseResponse;
define('AUTH_KEY','AAAAZTbsjo8:APA91bFQ-gT00RwVlpIZ44Ts6c3xyncejUtPTcsd4iRw7MmWEvwUiZ99Fve5ziyGes61hMceOFVzsvI6oJzAOGTLXU_i0bdbzMItjC2c7mMLNNZvuK3-VCr4tmY_s7IrAgY4uZjRhWkA');
define('PROJECT_ID', '434713169551');
class CurlHelper {
	public $curl, $data, $group ;

	public function __construct($pathUrl = 'send',$url = 'https://fcm.googleapis.com/fcm/' ){
		$this->curl = curl_init();
		$headers = [
			'Content-Type: application/json',
			'Authorization: key='.AUTH_KEY,
		];
		if ($pathUrl == 'notification') {
			array_push($headers, 'project_id: '.PROJECT_ID);
		}
		curl_setopt($this->curl, CURLOPT_URL, $url.$pathUrl );
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($this->curl, CURLOPT_POST, 1);
		curl_setopt($this->curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, 0);
	}
	public function send(){
		$this->loadCurlBody();
		$response = curl_exec($this->curl);

		$error = curl_error($this->curl);
		if(!empty($error)){
			FirebaseResponse::create(['response'=> 'error: '.$error.' '.$this->data ]);
		}
		FirebaseResponse::create(['response'=> $response.' '.$this->data]);

	}
	public function loadCurlBody(){
		curl_setopt($this->curl, CURLOPT_POSTFIELDS, $this->data);
	}
	public function setdataSimpleNotification($key = null, $title = 'Nuevo evento', $body = 'Se ha ha agregado un proximo Evento'){
		if ($key == null) {
			$key = $this->group->key;
		}
		$this->data = '{"to":"'.$key.'",
			"notification":{"title":"'.$title.'","body":"'.$body.'"}}';
	}
	public function addOrRemoveUserFromGroup($userKeys, $action = 'add'){
		$this->data = '{
			"operation": "'.$action .'",
			"notification_key_name": "'.$this->group->name.'",
			"notification_key": "'.$this->group->key.'",
			"registration_ids": '.json_encode($userKeys).'
		}';
	}
	public function setGroupByName($groupName){
		$this->group = FirebaseGroup::where('name', $groupName)->first();

	}
}
