<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Report;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\UserAbm;

class ReportApiController extends Controller
{
    public function getReport (Report $report){
        $file = Storage::get('reports/'.$report->report);
        $pathToFile = storage_path('app/public/reports/'.$report->report);

        $headers = array(
            'Content-Type: application/pdf',
        );
        return response()->download($pathToFile, $report->fileName, $headers) ;
    }
    public function getReportPaginate(Request $request, $from = 0, $limited = 10){
        $userAbm = UserAbm::where('email', $request->server('PHP_AUTH_USER'))
            ->first();

        $reportes = Report::where(function ($query) use ($userAbm) {
            HelperController::getUsersTypeCondition($query, $userAbm);
        })->orWhere(function ($query){
            $query->where('executive', 0)
            ->where('patner', 0);
        })
        ->offset(10 * ($from - 1 ) )
        ->limit($limited)
        ->orderBy('created_at', 'desc')
        ->get();
        HelperController::convertReportsFieldToBase64($reportes, 'reports/');

        return ResponseApi::response($reportes);

    }
}
