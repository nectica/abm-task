<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\ContactEntrace;
use App\Models\Contact;
use App\Models\StatusContact;
use App\Models\UserAbm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class ContactApiController extends Controller
{
    public function store(Request $request){

        $data = $request->all();
        $validator = Validator::make($data ,[

            'query' => 'required',
            'topic' => 'required',
            ]);
            if($validator->fails()){
                return ResponseApi::ResponseValidatorError( $validator->messages() ) ;
            }
        $status = StatusContact::first();
        $userAbm = UserAbm::where('email', $request->server('PHP_AUTH_USER'))->first();
        $data['email'] = $userAbm->email;
        $data['userAbm_id'] = $userAbm->id;
        $data['name'] = $userAbm->name;
        $data['company'] = $userAbm->company;
        $data['phone'] = $userAbm->phone;
        $data['status_contacts_id'] = isset($status->id) ? $status->id :  0;
        $contact = Contact::create($data);
        /* Mail::send(new ContactEntrace($contact) ); */
        return ResponseApi::responseToStore( $contact);
    }
}
