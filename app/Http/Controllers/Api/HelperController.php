<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\File;

class HelperController
{



    public static function convertImageFieldToBase64($objects,  $path, $nameField = 'image' )
    {
        foreach ($objects as $object) :
            $file = File::get(public_path('storage/'.$path . $object[$nameField]));
            $type = pathinfo($object[$nameField], PATHINFO_EXTENSION);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($file);
            $object[$nameField] = utf8_encode($base64);
        endforeach;
    }
    public static function convertReportsFieldToBase64($objects,  $path )
    {
        foreach ($objects as $object) :
            $fileImg = File::get(public_path('storage/'.$path . $object->image));
            $filePdf = File::get(public_path('storage/'.$path . $object->report));
            $typeImg = pathinfo($object->image, PATHINFO_EXTENSION);
            $typePdf = pathinfo($object->report, PATHINFO_EXTENSION);
            $base64Img = 'data:image/' . $typeImg . ';base64,' . base64_encode($fileImg);
            $base64Pdf =  'data:application/'.$typePdf.';base64,' .base64_encode($filePdf);
            $object->image = utf8_encode($base64Img);
            $object->report = utf8_encode($base64Pdf );
        endforeach;
    }
    public static function getUsersTypeCondition($query, $userAbm){
        if ($userAbm->executive && $userAbm->patner) {
            $query->where('executive', $userAbm->executive)
                ->orwhere('patner', $userAbm->patner);
        } else if ($userAbm->executive) {
            $query->where('executive', $userAbm->executive);
        } else if ($userAbm->patner){
            $query->where('patner', $userAbm->patner);
        }else {
            $query->where('executive', 0)
                ->where('patner', 0);
        }
        return $query;
    }
    public static function getConditionAllUsers($query, $detacado ){
        $query->where('executive', 0)
        ->where('patner', 0)
        ->where('destacado',$detacado);
    }

    public static function getPaginate($object, $all, $current , $limited){
        $total = ceil($all / $limited) ;
        $next = $total == $current ? $current : $current +1 ;
        $response[$object] = compact('total','current', 'next');
        return  $response;
    }
}
