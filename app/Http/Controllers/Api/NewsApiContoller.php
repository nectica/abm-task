<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\News;
use App\Models\UserAbm;
use Illuminate\Http\Request;

class NewsApiContoller extends Controller
{

	public function index(Request $request)
	{
		$userAbm = UserAbm::where('email', $request->server('PHP_AUTH_USER'))->first();
		$news = News::where('destacado', 0)
        ->where(function ($query) use ($userAbm) {
            if ($userAbm->executive && $userAbm->patner) {
                $query->where('executive', $userAbm->executive)
                    ->orwhere('patner', $userAbm->patner);
            } else if ($userAbm->executive) {
                $query->where('executive', $userAbm->executive);
            } else if ($userAbm->patner){
                $query->where('patner', $userAbm->patner);
            }else {
                $query->where('executive', 0)
                    ->where('patner', 0);
            }
        })
        ->orderBy('created_at', 'desc')
        ->get();
		return ResponseApi::response($news);
	}
	public function show(Request $request, $id)
	{
		$news = News::find($id);
		return ResponseApi::response($news);
	}


	public function getNewsDestacado(Request $request)
	{
		$userAbm = UserAbm::where('email', $request->server('PHP_AUTH_USER'))->first();
		$news = News::where('destacado', 1)
            ->where(function ($query) use ($userAbm) {
                if ($userAbm->executive && $userAbm->patner) {
                    $query->where('executive', $userAbm->executive)
                        ->orwhere('patner', $userAbm->patner);
                } else if ($userAbm->executive) {
                    $query->where('executive', $userAbm->executive);
                } else if ($userAbm->patner){
                    $query->where('patner', $userAbm->patner);
                }else {
                    $query->where('executive', 0)
                        ->where('patner', 0);
                }
            })
			->orderBy('created_at', 'desc')
			->get();

		return ResponseApi::response($news);
	}

	public function getNewsByUser(Request $request, $id){
		$news = News::where('user_id',$id)
			->orderBy('created_at', 'desc')
			->get();
		return ResponseApi::response($news);
	}
	public function filter(Request $request){
		$userAbm = UserAbm::where('email', $request->server('PHP_AUTH_USER'))->first();
		$event = News::where(function($query) use($request){
					$query->where('title', 'like', '%'.$request->title.'%')
						->orWhere('details', 'like', '%'.$request->title.'%');
			})    ->where(function ($query) use ($userAbm) {
                if ($userAbm->executive && $userAbm->patner) {
                    $query->where('executive', $userAbm->executive)
                        ->orwhere('patner', $userAbm->patner);
                } else if ($userAbm->executive) {
                    $query->where('executive', $userAbm->executive);
                } else if ($userAbm->patner){
                    $query->where('patner', $userAbm->patner);
                }else {
                    $query->where('executive', 0)
                        ->where('patner', 0);
                }
            })
			->where('destacado', 0 )
			->get();
		return ResponseApi::response($event);
    }

    function getNewspaginate(Request $request, $from = 0, $limited = 10){
        $userAbm = UserAbm::where('email', $request->server('PHP_AUTH_USER'))->first();

        $news = News::where('destacado', 0)
        ->where(function ($query) use ($userAbm){
            HelperController::getUsersTypeCondition($query, $userAbm);
        })
        ->orWhere(function ($query) {
            HelperController::getConditionAllUsers($query,0);
        })
        ->orderBy('created_at', 'desc')
        ->offset(10 * ($from-1 ))
        ->limit($limited)
        ->get();
        HelperController::convertImageFieldToBase64($news, $path = 'news/');

        return ResponseApi::response($news);
    }


}
