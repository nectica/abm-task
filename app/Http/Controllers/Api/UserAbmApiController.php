<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\MailPasswordRecovery;
use App\Models\Event;
use App\Models\MineralValor;
use App\Models\News;
use App\Models\Report;
use App\Models\UserAbm;
use DateInterval;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

class UserAbmApiController extends Controller
{
    const LIMITEDNEWS = 10;
    const  LIMITEDREPORT = 10;
	public function login(Request $request)
	{

        $userAbm = UserAbm::where('email', $request->server('PHP_AUTH_USER'))
            ->first();
        $userAbm->makeHidden([
            'password',
            'patner',
            'executive',
            'created_at',
            'updated_at',
            'auth_token',
            'expired_auth_token',
            'token',
            'expire_token'
        ]);

        $news = News::where(function ($query) use ($userAbm){
                HelperController::getUsersTypeCondition($query, $userAbm);
            })
            ->orWhere(function ($query) {
                HelperController::getConditionAllUsers($query,0);
            })
            ->orderBy('created_at', 'desc')
            ->offset(0)
            ->limit(self::LIMITEDNEWS)
            ->get();

		HelperController::convertImageFieldToBase64($news, $path = 'news/');
        $destacadoNews = News::where('destacado', 1)
            ->where(function ($query) use ($userAbm) {
                HelperController::getUsersTypeCondition($query, $userAbm);
            })->orWhere(function ($query) {
                HelperController::getConditionAllUsers($query,1);

            })
            ->orderBy('created_at', 'desc')
            ->get();
        HelperController::convertImageFieldToBase64($destacadoNews, $path = 'news/');
        $now =  Carbon::parse(new DateTime('now') )->toDateString();
        $events = Event::where('eventDate','>=', $now  )
            ->where(function ($query) use ($userAbm) {
                HelperController::getUsersTypeCondition($query, $userAbm);
            })->orWhere(function ($query) use ( $now){
                $query->where('executive', 0)
                ->where('patner', 0);
            })
			->orderBy('eventDate', 'ASC')
			->orderBy('created_at', 'desc')
            ->get();
		HelperController::convertImageFieldToBase64($events,  $path = 'event/');
        $destacadoEvents = Event::where('destacado', 1)
            ->where('eventDate','>=', $now  )
            ->where(function ($query) use ($userAbm) {
                HelperController::getUsersTypeCondition($query, $userAbm);
            })->orWhere(function ($query) use ( $now){
                $query->where('executive', 0)
                ->where('patner', 0)
                ->where('destacado',1)->where('eventDate','>=', $now  );
            })
            ->orderBy('eventDate', 'asc')
			->orderBy('created_at', 'desc')
            ->get();
		HelperController::convertImageFieldToBase64($destacadoEvents,  $path = 'event/');

        $minerales = MineralValor::where(function ($query){
			return $query->where('exportacion', '>', 0)->orWhere('valor', '>', 0);
		})->with('Mineral')->get()->groupBy(['Mineral.name', 'mes']);
        $reportes = Report::where(function ($query) use ($userAbm) {
            HelperController::getUsersTypeCondition($query, $userAbm);
        })->orWhere(function ($query){
            $query->where('executive', 0)
            ->where('patner', 0);
        })
        ->orderBy('created_at', 'desc')
        ->offset(0)
        ->limit(self::LIMITEDREPORT)
        ->get();
        $paginate = [];
        $totalReportes  = $this->getTotalReportes($userAbm);
        $paginate = [
            HelperController::getPaginate('news', $this->getTotalNews($userAbm), 1, self::LIMITEDNEWS),
            HelperController::getPaginate('report', $this->getTotalReportes($userAbm), 1, self::LIMITEDREPORT)
        ];
        HelperController::convertReportsFieldToBase64($reportes, 'reports/');
        $reportes->makeHidden(['report', 'fileName']);
        $user = $userAbm;
        $response = [compact('user', 'paginate', 'news', 'destacadoNews', 'events', 'destacadoEvents', 'minerales', 'reportes')];
		return ResponseApi::response($response);
	}
	public function index()
	{
		return ResponseApi::response(UserAbm::all());
	}
	public function store(Request $request)
	{
		$userAbm = UserAbm::where('email', $request->server('PHP_AUTH_USER'))->first();
		$data = $request->all();

		$validator = Validator::make($data, [
			'name' => 'required',
			'password' => 'required|min:6',
			'company' => 'required',
			'email' => 'required|email|unique:users_abm,email',
		]);
		if ($validator->fails()) {
			return ResponseApi::ResponseValidatorError($validator->messages());
		}
		$data['password'] = Hash::make($data['password']);
		$data['executive'] = 0;
		$data['patner'] = 0;
        $userAbm = UserAbm::create($data);
		return ResponseApi::responseToStore($userAbm);
    }
    public function recoveryPassword(Request $request){
        $data = $request->all();
        $validator = Validator::make($data,[
            'email' => 'required|email',
        ]
        );
        if ($validator->fails()) {
            return ResponseApi::ResponseValidatorError($validator->messages());
        }
        $userAbm = UserAbm::where('email', $request->email)->first();
        if($userAbm != null):
            $token = Str::random(60);
            $details = [
                'token' => $token,
                'name' => $userAbm->name,
                'id' => Crypt::encrypt( $userAbm->id)
            ];
            $expireDate = new DateTime('NOW');
            $expireDate->add(new DateInterval('P1D'));

            $userAbm->token = $token;
            $userAbm->expire_token = $expireDate;
            $userAbm->update();
            try {
                Mail::to($userAbm->email)->send(new MailPasswordRecovery($details));
            } catch (\Exception $e) {
                return response()->json(['status' => 'error',
                    'data' => [
                            'msg' => 'No Se ha podido enviar el email para la recuperacion de contrasena. Intente mas tarde o contacte un adminstrador'.$e->getMessage()
                            ]], 500);

            }
            return response()->json(['status' => 'successful', 'data' => ['msg' => 'Se le ha enviado un email para la recuperacion de contrasena.']]);
        else:
            return response()->json(['status' => 'error', 'data' => ['errors' => 'No se encontro usuario para con ese email.']], 404);
        endif;
    }


	public function update(Request $request)
	{
        $userAbm = UserAbm::where('email', $request->server('PHP_AUTH_USER'))
            ->first();
        $userAbm->makeHidden([
            'password',
            'patner',
            'executive',
            'created_at',
            'updated_at',
            'auth_token',
            'expired_auth_token', '
            token',
            'expire_token',
        ]);
		$data = $request->all();

		$validator = Validator::make($data, [
			'name' => 'required',
			'phone' => 'required',
			'company' => 'required',
			'jobTitle' => 'required',
			'email' => ['required', 'email', Rule::unique('users_abm')->where('email', $request->email)->ignore($userAbm)]
		]);
		if ($validator->fails()) {
			return ResponseApi::ResponseValidatorError($validator->messages());
        }
        /* unset($userAbm['password'], $userAbm['patner'], $userAbm['executive'], $userAbm['created_at'], $userAbm['updated_at']); */
        $mailBool = $userAbm->email != $request->email;
		$userAbm->name = $request->name;
		$userAbm->phone = $request->phone;
		$userAbm->company = $request->company;
        $userAbm->jobTitle = $request->jobTitle;
        $userAbm->email = $request->email;
        $userAbm->update();
        $userAbm->mailChanged = $mailBool;

		return ResponseApi::response($userAbm);
	}
	public function updatePassword(Request $request)
	{
		$userAbm = UserAbm::where('email', $request->server('PHP_AUTH_USER'))->first();
		$data = $request->all();

		$validator = Validator::make($data, [
			'old_password' => 'required',
			'password' => 'required|min:6|confirmed',
		]);
		if ($validator->fails()) {
			return ResponseApi::ResponseValidatorError($validator->messages());
        }
        if ( Hash::check($request->old_password, $userAbm->password) ) {
            $userAbm->password = Hash::make($request->password);
		    $userAbm->update();
        }else{
            return ResponseApi::ResponseValidatorError(['error' => 'La contrasena anterior no es correcta.']);
        }

		return ResponseApi::responseToStore($userAbm);
	}
	public function getUserByUserType($executive, $patner)
	{
		$userAbm = UserAbm::where('executive', $executive)->where('patner', $patner)->get();
		return ResponseApi::response($userAbm);
	}
	public function getUserByEmail(Request $request)
	{
		$data = $request->all();
		$validator = Validator::make($data, [
			'email' => 'required|email',
		]);
		if ($validator->fails()) {
			return ResponseApi::ResponseValidatorError($validator->messages());
		}
		$userAbm = UserAbm::where('email', $data['email'])->get();
		return ResponseApi::response($userAbm);
	}
	public function getUserById($id)
	{
		return ResponseApi::response(UserAbm::where('id', $id)->get());
    }

    public function getTotalNews($userAbm){
       return  News::where(function ($query) use ($userAbm) {
            HelperController::getUsersTypeCondition($query, $userAbm);
        })->orWhere(function ($query){
            $query->where('executive', 0)
            ->where('patner', 0);
        })->count();
    }
    public function getTotalReportes($userAbm){
        return  Report::where(function ($query) use ($userAbm) {
            HelperController::getUsersTypeCondition($query, $userAbm);
        })->orWhere(function ($query){
            $query->where('executive', 0)
            ->where('patner', 0);
        })->count();
     }
}
