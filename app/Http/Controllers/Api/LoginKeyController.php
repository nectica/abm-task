<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreLoginKey;
use App\Models\LoginKey;
use App\Models\UserAbm;
use Illuminate\Http\Request;

class LoginKeyController extends Controller
{
	public function index(UserAbm $user)
	{
		return ResponseApi::response($user->loginKeys->makeHidden(['userAbm_id', 'updated_at', 'created_at']));
	}
	public function create(StoreLoginKey $request)
	{
		$LoginKeyCreated = null;
		$userAbm = UserAbm::where('id', $request->user_id)->first();
		/*$userAbm->loginKeys->pluck('key')->all()*/
		$keyToInsert =  $this->checkMacAddress($request);
		if($keyToInsert){
			$LoginKeyCreated = $this->updateKeyLogin($keyToInsert, $request);
		}else {
			$LoginKeyCreated =	$this->createKeyToUser($request, $userAbm);
		}
		return ResponseApi::responseToStore($LoginKeyCreated);
	}
	public function createKeyToUser( $request, $userAbm){
		$values = $request->all();
		$values['userAbm_id'] = $values['user_id'];
		unset($values['user_id']);
		$LoginKey =	LoginKey::create($values);
		$this->addUserFromNotificationGroup('todos', [$request->key] );
		$this->checkWhichGroupToAddUser($userAbm, $request);
		return $LoginKey ;
	}
	public function checkMacAddress($request)
	{
		$LoginKey = LoginKey::where('mac', $request->mac)->first();
		return $LoginKey;
	}
	public function getArrayToSave($request)
	{
		$values = $request->all();
		$values['userAbm_id'] = $values['user_id'];
		unset($values['user_id']);
		return $values;
	}
	public function updateKeyLogin($keyToInsert, $request)
	{
		if ($keyToInsert->userAbm_id != $request->user_id ) {
			$this->checkWhichGroupToRemoveUser($keyToInsert->user, $request);
			$keyToInsert->update($this->getArrayToSave($request));
			$this->checkWhichGroupToAddUser($keyToInsert->user, $request);
			return $keyToInsert;
		}else if($keyToInsert->userAbm_id == $request->user_id && $keyToInsert->key != $request->key){
			$keyToInsert->update($this->getArrayToSave($request));
			$this->checkWhichGroupToRemoveUser($keyToInsert->user, $request);
			$this->checkWhichGroupToAddUser($keyToInsert->user, $request);
			$this->addUserFromNotificationGroup('todos', [$request->key] );


		}
		return $keyToInsert;
	}

	public function addUserFromNotificationGroup($groupName, $key,  $action = 'add')
	{
		$curl = new CurlHelper('notification');
		$curl->setGroupByName($groupName);
		$curl->addOrRemoveUserFromGroup( $key, $action);
		$curl->send();
	}
	public function checkWhichGroupToAddUser($userAbm, $request)
	{
		if ($userAbm->patner && $userAbm->executive) {
			$this->addUserFromNotificationGroup('socio-directiva', [$request->key]);
		} else if ($userAbm->patner) {
			$this->addUserFromNotificationGroup('socios', [$request->key]);
		} else if ($userAbm->executive) {
			$this->addUserFromNotificationGroup('directiva', [$request->key]);
		}
	}
	public function checkWhichGroupToRemoveUser($userAbm, $request){

		if ($userAbm->patner && $userAbm->executive) {
			$this->addUserFromNotificationGroup('socio-directiva', [$request->key], 'remove');
		} else if ($userAbm->patner) {
			$this->addUserFromNotificationGroup('socios',[$request->key],'remove');
		} else if ($userAbm->executive) {
			$this->addUserFromNotificationGroup('directiva', [$request->key], 'remove');
		}
	}
}
