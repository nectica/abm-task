<?php
namespace App\Http\Controllers\Api;

class ResponseApi {

    public static function response($data){
        if(isset($data)){
            return response()->json( array_merge(['status' => 'successfull'], compact('data')) );

        }else{
            return response()->json([ 'status' => 'error', 'data' => 'No found']);
        }
    }
    public static function responseToStore($modelTable){

        if ($modelTable->id) {
            return response()->json(['status' => 'successfull', 'data' => 'ok'], 201);
        }else{
            return response()->json([ 'status' => 'error', 'errors' => ['error' => 'No se pudo crear']]);
        }

    }
    public static function responseToUpdate($modelTable){
        return response()->json(['status' => 'successfull', 'data' => $modelTable], 200);


    }
    public static function ResponseValidatorError($message){
        return response()->json(['status' => 'error', 'errors' =>  $message]);

    }
}
