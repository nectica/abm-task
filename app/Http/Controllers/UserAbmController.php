<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\CurlHelper;
use App\Models\UserAbm;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Validation\ValidationException;

class UserAbmController extends Controller
{

    public function index()
    {
        $userAbm = UserAbm::sortable()->paginate(10);
        return  view('usersAbm.index', compact('userAbm') );
    }
    public function filter(Request $request)
    {
        $userAbm = UserAbm::sortable()
            ->where('name', 'like', '%'.$request->name.'%' )
            ->orWhere('email', 'like', '%'.$request->name.'%' )
            ->paginate(10);

        return  view('usersAbm.index', compact('userAbm') );
    }



    public function create()
    {
        return view('usersAbm.create');
    }


    public function store(Request $request)
    {
        $validator = Validator::make($request->all() ,[
			'name' => 'required',
			'password' => 'required|confirmed',
			'company' => 'required',
		]);

        $validator->validate();
        $userAbm = new UserAbm();
        $userAbm->name = $request->name;
        $userAbm->email = $request->email;
        $userAbm->password =  Hash::make($request->password) ;
        $userAbm->phone =  isset($request->phone) ? $request->phone : '' ;
        $userAbm->company =  $request->company ;
        $userAbm->jobTitle =  isset($request->jobTitle) ? $request->jobTitle : '' ;
        $userAbm->executive = false;
        $userAbm->patner = false;
        if( isset($request->userType) ):
            foreach($request->userType as $type):
                switch ($type):
                    case 'executive':
                        $userAbm->executive = true;
                        break;
                    case 'patner':
                        $userAbm->patner = true;
                        break;
                endswitch;

            endforeach;
        endif;
        $userAbm->save();
        return redirect()
            ->route('userAbm.index')
            ->with('success', 'Se ha creado correctamente el usuario.');
    }

    public function show( $id)
    {
        $userAbm = UserAbm::find($id);
        return view('usersAbm.show', compact('userAbm') );
    }


    public function edit( $id)
    {
        $userAbm = UserAbm::find($id);
        return view('usersAbm.update', compact('userAbm') );
    }


    public function update(Request $request,  $id)
    {

        $validator = Validator::make($request->all(), [
			'name' => 'required|max:255',
			'email' => 'required|email',
            'password' => 'confirmed'
		    ]
        );
        $validator->validate();
        $userAbmToUpdate = UserAbm::find($id) ;
        $userCopy = UserAbm::find($id) ;
        $userAbmToUpdate->name = $request->name;
        $userAbmToUpdate->email = $request->email;
        $userAbmToUpdate->email = $request->email;
        $userAbmToUpdate->phone =  $request->phone ;
        $userAbmToUpdate->company =  $request->company ;
        $userAbmToUpdate->jobTitle =  $request->jobTitle ;
        $userAbmToUpdate->executive = false;
        $userAbmToUpdate->patner = false;
        if (isset($request->password)):
            $userAbmToUpdate->password =  Hash::make($request->password) ;
        endif;
        if( isset($request->userType) ):
            foreach($request->userType as $type):
                switch ($type):
                    case 'executive':
                        $userAbmToUpdate->executive = true;
                        break;
                    case 'patner':
                        $userAbmToUpdate->patner = true;
                        break;
                endswitch;

            endforeach;
        endif;
		if ($userCopy->executive != $userAbmToUpdate->executive ||$userCopy->patner != $userAbmToUpdate->patner ) {
			foreach($userCopy->loginKeys as $keyLogin ){
				$beforeUpdatingGroup = $this->getGroupOfUser($userCopy);
				if ($beforeUpdatingGroup) {
					$this->addUserFromNotificationGroup($beforeUpdatingGroup, [$keyLogin->key], 'remove' ) ;
				}
				$afterUpdatingGroup = $this->getGroupOfUser($userAbmToUpdate);
				if ($afterUpdatingGroup) {
					$this->addUserFromNotificationGroup($afterUpdatingGroup, [$keyLogin->key]) ;

				}
			}
		}
        $userAbmToUpdate->update();
        return  redirect()->to($request->url)
            ->with('success', 'Se ha actualizado correctamente el usuario.');
    }

    public function destroy( $id)
    {
        $userAbm =  UserAbm::find($id);
        $userAbm->delete();
        return redirect()->route('userAbm.index')
            ->with('success', 'Se ha borrado correctamente el usuario.');
    }

    public function recoveryPassword($id, $token ){
        $userAbm =  UserAbm::find(Crypt::decrypt($id));
        return view('usersAbm.recoveryPassword', compact('userAbm', 'token', 'id'));

    }
    public function updatePassword(Request $request ){
        $userAbm = UserAbm::find(Crypt::decrypt($request->id) );
        $data = $request->all();
        $validator = Validator::make($data,[
            'password' => 'required|min:6|confirmed'
        ]);
        $validator->validate();
        $errors = ValidationException::withMessages([
            'token' => 'token ha expirado. por favor haga unas nueva solicitud'
        ]);
        $now = new DateTime('NOW');
        $expireTime = new  DateTime($userAbm->expire_token);
        if ( round(($now->getTimestamp() - $expireTime->getTimestamp())/3600)  < 0) {
            $userAbm->password = Hash::make($request->password);
            $userAbm->token = null;
            $userAbm->expire_token = null;
            $userAbm->update();
            return view('partials.alerts.success', ['success' => 'se ha restablecido la contraseña correctamete.']);
        }else {
            throw $errors;
        }
    }
	public function addUserFromNotificationGroup($groupName, $key,  $action = 'add')
	{
		$curl = new CurlHelper('notification');
		$curl->setGroupByName($groupName);
		$curl->addOrRemoveUserFromGroup( $key, $action);
		$curl->send();
	}

	public function getGroupOfUser($user){
		if ($user->patner && $user->executive) {
			return 'socio-directiva';
		} else if ($user->patner) {
			return 'socios';
		} else if ($user->executive) {
			return 'directiva';
		}
		return false;
	}
}
