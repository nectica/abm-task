<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\StatusContact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function __construct(){
		$this->middleware('permission:report-list|report-edit|report-create|report-delete', ['only' =>['index','show']]);
		$this->middleware('permission:report-edit', ['only' => ['edit','update']]);
	}
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = Contact::sortable()->orderBy('created_at', 'DESC')->orderBy('updated_at', 'ASC')->paginate(5);
        return  view('query.index', compact('contacts'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show( $id)
    {   $contact = Contact::find($id);
        return view('query.show', compact('contact'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $query)
    {
        $statuses = StatusContact::all();
        /* quede aqui, completar el formulario */
        return view('query.update',compact('query', 'statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $query)
    {
        $query->update($request->all());
		return redirect()->to($request->url)->with('success', 'Se ha actualizado el estado correctamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        //
    }
    public function filter(Request $request){
        $contacts = Contact::sortable()
            ->where('name', 'like', '%'.$request->title.'%')
            ->orWhere('company', 'like', '%'.$request->title.'%')
            ->orWhere('query', 'like', '%'.$request->title.'%')
            ->orWhere('topic', 'like', '%'.$request->title.'%')
            ->paginate(5);

        return  view('query.index', compact('contacts'));

    }
}
