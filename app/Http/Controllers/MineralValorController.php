<?php

namespace App\Http\Controllers;

use App\Models\Mineral;
use App\Models\MineralValor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class MineralValorController extends Controller
{

    public $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
        'Junio',
        'Julio',
        'Agosto',
        'Septiembre',
        'Octubre',
        'Noviembre',
        'Diciembre',
    ];
    function  __construct(){
        $this->middleware('permission:mineral-list|mineral-create|mineral-edit|mineral-delete', ['only' => ['index','show']]);
		$this->middleware('permission:mineral-create', ['only' => ['create','store']]);
		$this->middleware('permission:mineral-edit', ['only' => ['edit','update']]);
		$this->middleware('permission:mineral-delete', ['only' => ['destroy']]);
    }
    public function index()
    {
        $meses = $this->meses;
        $valores = MineralValor::sortable()
            ->orderBy('created_at', 'desc')
            ->paginate(5);
        return view('mineralValor.index', compact('valores', 'meses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $minerales = Mineral::all();
        $meses = $this->meses;
        $anio = date('Y');
        return view('mineralValor.create', compact('minerales', 'meses', 'anio'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $errors = ValidationException::withMessages([
            'valor' => 'se debe cargar un valor correcto el precio  y exportaciones.',
        ]);
        $errorsIfExists = ValidationException::withMessages([
            'valor' => 'ya existe registrado el valor del mineral para el mes y año' ,
            'exportacion' => 'ya existe registrado el valor del mineral para el mes y año.',
                ]);
        $countSave = 0;
        $data =  $request->all();
        Validator::extend('check_array', function ($attribute, $value, $parameters, $validator) {
            foreach($value as $val){
                if ($val == null) {
                    return false;
                }
            }
            return true;
       });

        $validator = Validator::make($data, [
            'mes' => 'required',
            'ano' => 'required',
            'mineral_id' => 'required',
            'valor' => 'check_array',
            'exportacion' => 'check_array',

        ]);
        $validator->validate();
        for ($i=0; $i < count($request->mineral_id); $i++):
            $mineralValor = new MineralValor();
            $mineralValor->ano = $request->ano ;
            $mineralValor->mes = $request->mes ;
            $mineralValor->mineral_id = $request->mineral_id[$i];
            if( isset($request->valor[$i])  ):
                $mineralValor->valor = (Double) $request->valor[$i];
            endif;
            if( isset($request->exportacion[$i]) ):
                $mineralValor->exportacion = (integer) $request->exportacion[$i];
            endif;
            if( ($mineralValor->valor !== null && $mineralValor->valor  >= 0  ) && ($mineralValor->exportacion !== null && $mineralValor->exportacion  >=0 )):
                $price= MineralValor::where('ano', $mineralValor->ano)
                    ->where('mes',$mineralValor->mes)
                    ->where('mineral_id', $mineralValor->mineral_id)
                    ->first();
                if ( isset($price) ) {
                    throw $errorsIfExists;
                }else{
                    $mineralValor->save();
                    $countSave++;
                }
            endif;
        endfor;

        if ( $countSave == 0) {
            throw $errors;
        }
        return  redirect()->route('mineralesValor.index')
            ->with('success','Se registraron los precios y exportacion de los minerales.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MineralValor  $mineralValor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mineralValor =  MineralValor::find($id);
        return view('mineralValor.show', compact('mineralValor') );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MineralValor  $mineralValor
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        $mineralValor =  MineralValor::find($id);
        $mineral = Mineral::find($mineralValor->mineral_id);
        $meses = $this->meses;
        $anio = date('Y');
        return view('mineralValor.update', compact('mineralValor', 'mineral', 'meses', 'anio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MineralValor  $mineralValor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id ){
        $mineralValor = MineralValor::find($id);
        $mes = $request->mes;
        $anio = $request->ano;
        $mineral = $request->mineral_id;
        $data = $request->all();
        $validator = Validator::make($data,[
            'mes' => 'required',
            'ano' => 'required',
            'mineral_id' => 'required',
			'valor' => 'required',
            'exportacion' => ['required',Rule::unique('minerales_valores')->ignore($mineralValor)->where(function($query)use($mes, $anio, $mineral){
                return $query->where('mes',$mes)->where('ano', $anio)->where('mineral_id', $mineral) ;
            } ),
        ],
        ]);
        $validator->validate();
        $mineralValor->mes = $mes;
        $mineralValor->ano = $anio;
        $mineralValor->valor = $request->valor;
        $mineralValor->exportacion = $request->exportacion;
        $mineralValor->update();
        return redirect()->to($request->url)->with('success', 'Se actualizo correctamente el valor del mineral.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MineralValor  $mineralValor
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mineralValor = MineralValor::find($id);
        $mineralValor->delete();
        return redirect()->route('mineralesValor.index')->with('success', 'Se elimino correctamente el valor del mineral.');
    }
}
