<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Event;
use App\Models\Report;
use App\Models\UserAbm;
use DateInterval;
use DateTime;
use Illuminate\Http\Request;

class HomeController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $interval = new DateInterval('P1W');
        $now = new  DateTime('now');

        $lastWeek = new  DateTime('now');
        $nextWeek = new  DateTime('now');
        $now->setTime(0,0);
        $lastWeek->setTime(0,0);
        $lastWeek->sub($interval);
        $nextWeek->setTime(23, 59, 59);
        $nextWeek->add($interval);
        $reports = Report::where('created_at', '>=', $lastWeek)->count();
        $users = UserAbm::where('created_at', '>=', $lastWeek)->count();
        $queries = Contact::where('created_at', '>=', $lastWeek)->count();
        $events = Event::where('eventDate','>=', $now)->where('EventDate','<=', $nextWeek)->count();
        return view('home', compact('reports', 'users', 'queries', 'events' ));
    }
    public function getEventsHome(){
        $interval = new DateInterval('P1W');
        $now = new  DateTime('now');
        $nextWeek = new  DateTime('now');
        $now->setTime(0,0);
        $nextWeek->setTime(23, 59, 59);
        $nextWeek->add($interval);
        $events = Event::sortable()->where('eventDate','>=', $now)->where('EventDate','<=', $nextWeek)->paginate(5);
        return view('event.index', compact('events'));

    }
    public function getReportsHome(){
        $interval = new DateInterval('P1W');
        $lastWeek = new  DateTime('now');
        $lastWeek->setTime(0,0);
        $lastWeek->sub($interval);
        $reports = Report::sortable()->where('created_at', '>=', $lastWeek)->paginate(5);
        return view('report.index', compact('reports'));

    }
    public function getUsersHome(){
        $interval = new DateInterval('P1W');
        $lastWeek = new  DateTime('now');
        $lastWeek->setTime(0,0);
        $lastWeek->sub($interval);
        $userAbm = UserAbm::sortable()->where('created_at', '>=', $lastWeek)->paginate(5);
        return view('usersAbm.index', compact('userAbm'));

    }
    public function getQueriesHome(){
        $interval = new DateInterval('P1W');
        $lastWeek = new  DateTime('now');
        $lastWeek->setTime(0,0);
        $lastWeek->sub($interval);
        $contacts = Contact::sortable()->where('created_at', '>=', $lastWeek)->paginate(5);
        return view('query.index', compact('contacts'));
    }
}
