<?php

namespace App\Http\Controllers;

use App\Models\Report;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ReportController extends Controller
{

    function  __construct(){
        $this->middleware('permission:report-list|report-create|report-edit|report-delete', ['only' => ['index','show']]);
		$this->middleware('permission:report-create', ['only' => ['create','store']]);
		$this->middleware('permission:report-edit', ['only' => ['edit','update']]);
		$this->middleware('permission:report-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reports = Report::sortable()->paginate(5);
        return view('report.index', compact('reports'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('report.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $report = new Report();
        $report->title = $request->title;
		$report->copete = $request->copete;
        $report->user_id = $request->user()->id;
        $report->executive = false;
        $report->patner = false;
        if($request->userType):
            foreach($request->userType as $type):
                switch ($type):
                    case 'executive':
                        $report->executive = true;
                        break;
                    case 'patner':
                        $report->patner = true;
                        break;
                endswitch;
            endforeach;
        endif;
        Validator::extend('check_array', function ($attribute, $value, $parameters, $validator) {

            foreach($value as $val){
                if ($val != null) {
                    return true;
                }
            }
            return false;
       });
		$validator = Validator::make(request()->all(), [
			'reporte' => 'required|mimes:pdf',
			'copete' => 'required|max:255',
            'title' => 'required|max:255',
            'image' => 'mimes:jpeg,bmp,png',
            'userType' => 'required'

		]);
		$validator->validate();
		if ($request->hasFile('reporte') && $request->file('reporte')->isValid()) {
			$fileOriginalName = $request->file('reporte')->getClientOriginalName();
			$nameFile = date('Y_m_d_H_i_s') . '_' . $fileOriginalName;
            $path = $request->file('reporte')->storeAs('reports', $nameFile);
            $report->fileName = $fileOriginalName;
			$report->report = $nameFile;
        }
        if ($request->hasFile('image') && $request->file('image')->isValid()) {
			$fileOriginalName = $request->file('image')->getClientOriginalName();
			$nameFile = date('Y_m_d_H_i_s') . '_' . $fileOriginalName;
            $path = $request->file('image')->storeAs('reports', $nameFile);
            $report->image = $nameFile;
		}else{
            $report->image = 'default.png';
        }
		$report->save();
		return redirect()->route('report.index')->with('success', 'Se ha creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function show(Report $report)
    {
        return view('report.show', compact('report'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function edit(Report $report)
    {
        return view('report.update', compact('report'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Report $report)
    {

        $report->title = $request->title;
		$report->copete = $request->copete;
        $report->user_id = $request->user()->id;
        $report->executive = false;
        $report->patner = false;
        if($request->userType):
            foreach($request->userType as $type):
                switch ($type):
                    case 'executive':
                        $report->executive = true;
                        break;
                    case 'patner':
                        $report->patner = true;
                        break;
                endswitch;
            endforeach;
        endif;
		$validator = Validator::make(request()->all(), [
			'copete' => 'required|max:255',
            'title' => 'required|max:255',
            'userType' => 'required',
            'image' => 'mimes:jpeg,bmp,png',
			'reporte' => 'mimes:pdf',

		]);
		$validator->validate();
		if ($request->hasFile('reporte') && $request->file('reporte')->isValid()) {
            $validator = Validator::make(request()->all(), [
				'reporte' => 'required|mimes:pdf',

			]);
            $validator->validate();
            Storage::delete('reports/'.$report->report);
            $fileOriginalName = $request->file('reporte')->getClientOriginalName();
			$nameFile = date('Y_m_d_H_i_s') . '_' . $fileOriginalName;
            $path = $request->file('reporte')->storeAs('reports', $nameFile);
            $report->fileName = $fileOriginalName;
			$report->report = $nameFile;
        }
        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            Storage::delete('reports/'.$report->image);
			$fileOriginalName = $request->file('image')->getClientOriginalName();
			$nameFile = date('Y_m_d_H_i_s') . '_' . $fileOriginalName;
            $path = $request->file('image')->storeAs('reports', $nameFile);
            $report->image = $nameFile;
		}
        $report->update();
        return  redirect()->to($request->url)->with('success', 'Se ha actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function destroy(Report $report)
    {
        Storage::delete('reports/'.$report->report);
        $report->delete();
        return redirect()->route('report.index')->with('success', 'Se ha eliminado correctamente');
    }
    function filter(Request $request){
        $reports = Report::sortable()
        ->where('title', 'like', '%'.$request->title.'%' )
        ->orWhere('copete', 'like', '%'.$request->title.'%' )
        ->paginate(5);
        return view('report.index', compact('reports'));
    }
    function download(Report $report){
        $pathToFile = Storage::path('reports/'.$report->report);
        $name = $report->fileName;
        $headers = ["Content-type:application/pdf"];
        return response()->download($pathToFile, $name, $headers);

    }
}
