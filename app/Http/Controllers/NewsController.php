<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\CurlHelper;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageServiceProvider;
use Image;

class NewsController extends Controller
{
	function __construct(){
		$this->middleware('permission:news-list|news-create|news-edit|news-delete', ['only' => ['index','show']]);
		$this->middleware('permission:news-create', ['only' => ['create','store']]);
		$this->middleware('permission:news-edit', ['only' => ['edit','update']]);
		$this->middleware('permission:news-delete', ['only' => ['destroy']]);
	}
	public function index()
	{
		$news = News::sortable()->orderBy('created_at', 'DESC')->paginate(5);
		return view('news.index', compact('news'));
    }



	public function create()
	{
		return view('news.create');
	}
	public function store(Request $request)
	{
		$newsToSave = new News();
		$newsToSave->title = $request->title;
		$newsToSave->details = $request->description;
		$newsToSave->user_id = $request->user()->id;
        $newsToSave->executive = false;
        $newsToSave->patner = false;
        $newsToSave->destacado = $request->destacado ? true : false ;
		$newsToSave->notification = $request->notification ? true: false;
		$validator = Validator::make(request()->all(), [
			'img' => 'required|mimes:jpeg,bmp,png|max:512|dimensions:min_width=640',
			'description' => 'required',
            'title' => 'required|max:255',
		]);
        $validator->validate();
        if( isset($request->userType) ):
            foreach($request->userType as $type):
                switch ($type):
                    case 'executive':
                        $newsToSave->executive = true;
                        break;
                    case 'patner':
                        $newsToSave->patner = true;
                        break;
                endswitch;
            endforeach;
        endif;
		if ($request->hasFile('img') && $request->file('img')->isValid()) {
            list($width, $height, $typeAtt, $att) = getimagesize($request->file('img'));
            $extesion = $request->file('img')->extension();
			$fileOriginalName = $request->file('img')->getClientOriginalName();
			$nameFile = date('Y_m_d_H_i_s') . '_' . $fileOriginalName.'.'.$extesion;
            $path = $request->file('img')->storeAs('news', $nameFile);
            $newsToSave->image = $nameFile;
            $widthDefault = 640;
            $image = Image::make($request->file('img'));

            if ($width > $widthDefault) {
                $image->resize($widthDefault, null,  function ($constraint) {
                    $constraint->aspectRatio();
                })->encode($extesion);
                $image->save(storage_path('app/public/news/'.$nameFile) );
            }
		}
		$newsToSave->save();
		$this->pushNotification($newsToSave);

		return redirect()->route('news.index')->with('success', 'Se ha creado correctamente');
	}
	public function show(News $news)
	{
		return view('news.show', compact('news'));
	}
	public function edit(News $news)
	{
		return view('news.update', compact('news'));
	}
	public function update(Request $request)
	{

		$newsToUpdate = News::find($request->id);
		$newsToUpdate->title = $request->title;
        $newsToUpdate->details = $request->description;
        $newsToUpdate->executive = false;
        $newsToUpdate->patner = false;
        $newsToUpdate->destacado = $request->destacado ? true : false ;
		$newsToUpdate->notification = $request->notification ? true: false;

		$validator = Validator::make(request()->all(), [
			'description' => 'required',
			'title' => 'required|max:255',
		]);
        $validator->validate();
        if(isset($request->userType) ):
            foreach($request->userType as $type):
                switch ($type):
                    case 'executive':
                        $newsToUpdate->executive = true;
                        break;
                    case 'patner':
                        $newsToUpdate->patner = true;
                        break;
                endswitch;
            endforeach;
        endif;
		if ($request->hasFile('img') && $request->file('img')->isValid()) {
            list($width, $height, $typeAtt, $att) = getimagesize($request->file('img'));
            $extesion = $request->file('img')->extension();
			$validator = Validator::make(request()->all(), [
                'img' => 'mimes:mimes:jpeg,bmp,png|max:500|dimensions:min_width=640',

			]);
			$validator->validate();
			Storage::delete('news/' . $newsToUpdate->image);
			$fileOriginalName = $request->file('img')->getClientOriginalName();
			$nameFile = date('Y_m_d_H_i_s') . '_' . $fileOriginalName.'.'.$extesion;
			$path = $request->file('img')->storeAs('news', $nameFile);
            $newsToUpdate->image = $nameFile;
            $widthDefault = 640;
            $image = Image::make($request->file('img'));
            if ($width > $widthDefault) {
                $image->resize($widthDefault,null,  function ($constraint) {
                    $constraint->aspectRatio();
                })->encode($extesion);
                $image->save(storage_path('app/public/news/'.$nameFile) );
            }
		}

		$newsToUpdate->update();
		$this->pushNotification($newsToUpdate);
		return  redirect()->to($request->url)->with('success', 'Se ha Actualizado correctamente');
	}
	public function destroy(News $news)
	{
		Storage::delete('news/' . $news->image);
		$news->delete();
		return redirect()->route('news.index')->with('success', 'Se ha borrado correctamente');
    }
    public function filter(Request $request)
    {
        $news = News::sortable()
            ->where('title', 'like', '%'.$request->title.'%' )
            ->orWhere('details', 'like', '%'.$request->title.'%' )
            ->paginate(10);

        return  view('news.index', compact('news') );
    }

	public function pushNotification($news, $udpated = false){

		if ($news->notification) {
			$description = strlen($news->details) > 255 ? substr($news->details, 0, 255) . '...' : $news->details;
			$title = strlen($news->title) > 255 ? substr($news->title, 0, 255) . '...' : $news->title;
			if($news->executive && $news->patner ){
				$this->curlSendNoticiation('socio-directiva', $udpated, $title, $description);
				$this->curlSendNoticiation('socios', $udpated, $title, $description);
				$this->curlSendNoticiation('directiva', $udpated, $title, $description);
			}else if($news->executive){
				$this->curlSendNoticiation('directiva', $udpated, $title, $description);
				$this->curlSendNoticiation('socio-directiva', $udpated, $title, $description);
			}else if( $news->patner ){
				$this->curlSendNoticiation('socios', $udpated, $news->title, $description);
				$this->curlSendNoticiation('socio-directiva', $udpated, $title, $description);
			}else {
				$this->curlSendNoticiation('todos', $udpated, $title, $description);
			}
		}
	}

	public function curlSendNoticiation($groupName, $udpated, $title, $description ){
		$curl = new CurlHelper();
		$curl->setGroupByName($groupName);
		/* if ($udpated) {
			$curl->setdataSimpleNotification(null, $title, $description);
		}else {
			$curl->setdataSimpleNotification(null, $title, $description);
		} */
		$curl->setdataSimpleNotification(null, $title, $description);
		$curl->send();
	}
}
