<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\CurlHelper;
use App\Models\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Image;


class EventController extends Controller
{
    function __construct(){
		$this->middleware('permission:event-list|event-create|event-edit|event-delete', ['only' => ['index','show']]);
		$this->middleware('permission:event-create', ['only' => ['create','store']]);
		$this->middleware('permission:event-edit', ['only' => ['edit','update']]);
		$this->middleware('permission:event-delete', ['only' => ['destroy']]);
	}
	public function index()
	{
        $events = Event::sortable()->orderBy('created_at', 'DESC')->paginate(5);
		return view('event.index', compact('events'));
	}
    public function filter(Request $request)
    {
        $events = Event::sortable()
            ->where('title', 'like', '%'.$request->title.'%' )
            ->orWhere('details', 'like', '%'.$request->title.'%' )
            ->paginate(10);

        return view('event.index', compact('events'));
    }
	public function create()
	{
		return view('event.create');
	}
	public function store(Request $request)
	{
		$eventToSave = new Event();
		$eventToSave->title = $request->title;
		$eventToSave->details = $request->description;
		$eventToSave->eventDate = $request->eventDate;
        $eventToSave->user_id = $request->user()->id;
        $eventToSave->executive = false;
        $eventToSave->patner = false;
        $eventToSave->destacado = $request->destacado ? true : false ;
        $eventToSave->notification = $request->notification ? true : false ;

		$validator = Validator::make(request()->all(), [
			'img' => 'required|mimes:jpeg,bmp,png|max:500|dimensions:min_width=640',
			'description' => 'required',
			'title' => 'required|max:255',
			'eventDate' => 'required|after_or_equal:'.date('Y-m-d'),
		]);
        $validator->validate();
        if( isset($request->userType) ):
            foreach($request->userType as $type):
                switch ($type):
                    case 'executive':
                        $eventToSave->executive = true;
                        break;
                    case 'patner':
                        $eventToSave->patner = true;
                        break;
                endswitch;
            endforeach;
        endif;
		if ($request->hasFile('img') && $request->file('img')->isValid()) {
			list($width, $height, $typeAtt, $att) = getimagesize($request->file('img'));
            $extesion = $request->file('img')->extension();
            $fileOriginalName = $request->file('img')->getClientOriginalName();
			$nameFile = date('Y_m_d_H_i_s') . '_' . $fileOriginalName.'.'.$extesion;
			$path = $request->file('img')->storeAs('event', $nameFile);
            $eventToSave->image = $nameFile;
            $widthDefault = 640;
            $image = Image::make($request->file('img'));
            if ($width > $widthDefault) {
                $image->resize($widthDefault, null,  function ($constraint) {
                    $constraint->aspectRatio();
                })->encode($extesion);
                $image->save(storage_path('app/public/event/'.$nameFile) );
            }
		}
		$eventToSave->save();
		$this->pushNotification($eventToSave);
		return redirect()->route('event.index')->with('success', 'Se ha creado correctamente');
	}
	public function show(Event $event)
	{
		return view('event.show', compact('event'));
	}
	public function edit(Event $event)
	{
		return view('event.update', compact('event'));
	}
	public function update(Request $request, Event $event)
	{
		$eventToUpdate = $event;
		$eventToUpdate->title = $request->title;
		$eventToUpdate->details = $request->description;
        $eventToUpdate->eventDate = $request->eventDate;
        $eventToUpdate->executive = false;
        $eventToUpdate->patner = false;
        $eventToUpdate->destacado = $request->destacado ? true : false ;
		$eventToUpdate->notification = $request->notification ? true : false ;

		$validator = Validator::make(request()->all(), [
			'description' => 'required',
			'title' => 'required|max:255',
			'eventDate' => 'required|after_or_equal:'.date('Y-m-d'),
		]);
        $validator->validate();
        if(isset($request->userType) ):
            foreach($request->userType as $type):
                switch ($type):
                    case 'executive':
                        $eventToUpdate->executive = true;
                        break;
                    case 'patner':
                        $eventToUpdate->patner = true;
                        break;
                endswitch;
            endforeach;
        endif;
		if ($request->hasFile('img') && $request->file('img')->isValid()) {

			$validator = Validator::make(request()->all(), [
                'img' => 'mimes:jpeg,bmp,png|max:500|dimensions:min_width=640',
            ]);
			$validator->validate();

			Storage::delete('event/' . $eventToUpdate->image);
			list($width, $height, $typeAtt, $att) = getimagesize($request->file('img'));
            $extesion = $request->file('img')->extension();
            $fileOriginalName = $request->file('img')->getClientOriginalName();
			$nameFile = date('Y_m_d_H_i_s') . '_' . $fileOriginalName.'.'.$extesion;
			$path = $request->file('img')->storeAs('event', $nameFile);
            $eventToUpdate->image = $nameFile;
            $widthDefault = 640;
            $image = Image::make($request->file('img'));
            if ($width > $widthDefault) {
                $image->resize($widthDefault,null,  function ($constraint) {
                    $constraint->aspectRatio();
                })->encode($extesion);
                $image->save(storage_path('app/public/event/'.$nameFile) );
            }
        }
		/* $changedFields = $eventToUpdate->getDirty();
		if(key_exists('eventDate',$changedFields)  ){
			$this->pushNotification($eventToUpdate, true);
		} */
		$eventToUpdate->update();
		$this->pushNotification($eventToUpdate, true);
		return  redirect()->to($request->url)->with('success', 'Se ha Actualizado correctamente');
	}
	public function destroy(Event $event)
	{
		/* Storage::delete('event/' . $event->image); */
		$event->delete();
		return redirect()->route('event.index')->with('success', 'Se ha borrado correctamente');
	}
	public function pushNotification($event, $udpated = false){
		if ($event->notification) {
			$description = strlen($event->details) > 255 ? substr($event->details, 0, 255) . '...' : $event->details;
			$title = strlen($event->title) > 255 ? substr($event->title, 0, 255) . '...' : $event->title;
			if($event->executive && $event->patner ){
				$this->curlSendNoticiation('socio-directiva', $udpated, $title, $description);
				$this->curlSendNoticiation('socios', $udpated, $title, $description);
				$this->curlSendNoticiation('directiva', $udpated, $title, $description);
			}else if($event->executive){
				$this->curlSendNoticiation('directiva', $udpated, $title, $description);
				$this->curlSendNoticiation('socio-directiva', $udpated, $title, $description);
			}else if( $event->patner ){
				$this->curlSendNoticiation('socios', $udpated, $title, $description);
				$this->curlSendNoticiation('socio-directiva', $udpated, $title, $description);
			}else {
				$this->curlSendNoticiation('todos', $udpated, $title, $description);
			}
		}
	}

	public function curlSendNoticiation($groupName, $udpated, $title, $description ){
		$curl = new CurlHelper();
		$curl->setGroupByName($groupName);
		/* if ($udpated) {
			$curl->setdataSimpleNotification(null, $title, $description);
		}else {
			$curl->setdataSimpleNotification(null, $title, $description);
		} */
		$curl->setdataSimpleNotification(null, $title, $description);
		$curl->send();
	}
}
