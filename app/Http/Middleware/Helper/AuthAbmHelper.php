<?php
namespace App\Http\Middleware\Helper;

use App\Models\UserAbm;
use DateInterval;
use DateTime;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class AuthAbmHelper {

    public static function validateUserApi(Request $request){
        $usr = $request->server('PHP_AUTH_USER');
        $pass = $request->server('PHP_AUTH_PW');
        $userAbm = UserAbm::where('email', $usr)->first();
        if( isset($userAbm)  ){
            if( $pass == null ){
                abort(403, 'contrasena es requerido. ');
            }else if( !Hash::check($pass, $userAbm->password ) ){
                abort(403, 'contrasena es incorrecta.');

            }
        }else {
            abort(403, 'usuario no existe.');
        }
    }
}



            /* else{
                if (  isset($userAbm->auth_token) &&  $pass !=$userAbm->auth_token ) {
                    $token = uniqid( base64_encode(Str::random(30)) );
                    $now = new DateTime('now');
                    $next = new DateInterval('P1W');
                    $now->add($next);
                    $userAbm->auth_token = $token;
                    $userAbm->expired_auth_token  = $now;
                    $userAbm->update();



                    abort(403, 'token invalido.');

                }else{

                }

            } */
