<?php

namespace App\Http\Middleware;

use App\Http\Middleware\Helper\AuthAbmHelper;
use Closure;
use Illuminate\Http\Request;

class CheckAuthApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        AuthAbmHelper::validateUserApi($request);
        return $next($request);
    }
}
