window.onload = () =>{
    const conditionIfMatch = (value, key) => value.indexOf(key) > -1;
    let $input =  document.querySelector('#searchInput') ;
    let $tableRows= document.querySelector('#bodyTable').querySelectorAll('tr');
    $input.addEventListener('keyup', function(evt) {
        const value = evt.target.value.toUpperCase();
        $tableRows.forEach(tr => {
            try {
                tr.style.display = 'none';
                tr.querySelectorAll('td').forEach( td => {
                    if(!td.dataset.excludes ){
                        if(td.children.length){
                            for (const childrenTd  of td.children) {
                                if(conditionIfMatch(childrenTd.innerHTML.toUpperCase(), value )){
                                    tr.style.display = 'table-row';
                                    throw BreakException;
                                }
                            }
                        }else{
                            if (conditionIfMatch(td.innerHTML.toUpperCase(), value )) {
                                tr.style.display = 'table-row';
                                throw BreakException;

                            }
                        }
                    }
                });
            } catch (error) {

            }

    });


    });
}
