window.onload = () => {
    const preventDelete = (event) =>{
        if(!confirm('¿Desea Eliminar el elemento?')){
            event.preventDefault();
        }
    }
    const formsDelete = document.getElementsByName('form-delete');
    for (const form of formsDelete) {
        const button = form.getElementsByTagName('button')[0] || form.querySelectorAll('.btn-danger')[0];
        button.disabled = false;
        form.addEventListener('submit', preventDelete );
    }

}
