@extends('layouts.layout')
@section('title', 'Pagina principal de evento')
@section('content')
<div class="row justify-content-center">
    @if ($message = Session::get('success'))
        <div class=" col-6  mt-2 alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
</div>
<div class="row justify-content-between m-3" >
    <div class="col-lg-3 offset-1">
        <h2 class="font-color-caem">Eventos</h2>
    </div>
    <div class="col-lg-3 offset-3">
        <form action="{{ route('event.filter') }}" method="POST">
            @csrf
            <div class="input-group ">
                <input type="text" class="form-control" id="searchInput" name="title"
                    placeholder="Filtrar por nombre o descripción">
                <div class="input-group-append">
                    <button class="btn btn-primary " type="submit">Filtrar</button>
                </div>
            </div>
        </form>
    </div>
    @can('event-create')
        <div class="col-lg-2">
            <a href="{{route('event.create')}}" type="button" class="btn btn-success-caem">Crear evento</a>
        </div>
    @endcan
</div>
<div class="row justify-content-center">
	<div class="col-lg-10 table-responsive">
		<table class="table  table-bordered ">
			<thead >
				<tr>
					<th scope="col">@sortablelink('title', 'Titulo')</th>
					<th scope="col">@sortablelink('details', 'Descripción') </th>
					<th scope="col"> @sortablelink('eventDate', 'Fecha') </th>
					<th scope="col">@sortablelink('image','Imagen') </th>
					<th scope="col">@sortablelink('executive', 'Comisión Directiva') </th>
					<th scope="col">@sortablelink('patner', 'Socio') </th>
					<th scope="col">@sortablelink('destacado', 'destacado') </th>
					<th scope="col"></th>
				</tr>
			</thead>
			<tbody>
				@foreach ($events as $eventItem)

					<tr>
						<td><a href="{{route('event.show', $eventItem->id )}}" >{{$eventItem->title}}</a></td>
						<td>{{ strlen($eventItem->details) > 255  ? substr($eventItem->details,0 , 255).'...' : $eventItem->details}}</td>
                        <td>{{ date_format( DateTime::createFromFormat('Y-m-d',$eventItem->eventDate) ,'d/m/Y')}}</td>
                        <td>{{$eventItem->image}}</td>
                        <td>{{ $eventItem->executive == 1 ? 'Si' : 'No' }}</td>
                        <td>{{ $eventItem->patner == 1 ? 'Si' : 'No' }}</td>
                        <td>{{$eventItem->destacado ? 'Si' : 'No' }}</td>
						<td>
                            @can('event-edit')
                                <a href="{{route('event.edit', $eventItem->id )}}" type="button" class="btn btn-outline-primary  btn-sm  my-1"><i class="fas fa-edit"></i></a>
                            @endcan
                            @can('event-delete')
                                <form name="form-delete" action="{{route('event.delete', $eventItem->id )}}" method="POST" >
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-outline-danger btn-sm" disabled><i class="fas fa-trash-alt"></i></button>
                                </form>
                            @endcan

						</td>
					</tr>
					@endforeach
			</tbody>
		</table>
		{{$events->appends(\Request::except('page'))->render()}}
	</div>

</div>


@endsection
<script src="{{ asset('js/delete.js') }}" defer></script>

