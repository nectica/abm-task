@extends('layouts.layout')
@section('title', 'Crear Noticia')
@section('content')
<div class="row justify-content-center mt-3">
    <div class="col-8">
        <h3>
            Actualizar Noticia
        </h3>

    </div>
	@if ($errors->any())
		<div class="col-8">
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
				</ul>
			</div>
		</div>
	@endif
</div>
	<div class="row justify-content-center mt-3">
		<div class="col-8">
			<form method="POST" action="{{route('event.update',$event->id)}}"  enctype="multipart/form-data">
                @csrf
                @method('PUT')
				<input type="hidden" name="url" value="{{ old('url', url()->previous())  }}" />

                <input type="hidden" name="id" value="{{$event->id}}">
				<div class="form-group">
					<label for="title">Titulo</label>
					<input type="text" class="form-control" id="title" name="title" value="{{old('title', $event->title)}}">
				</div>
				<div class="form-group">
					<label for="description">Detalles</label>
					<textarea class="form-control" id="description" name="description" rows="3">{{old('description',$event->details) }}</textarea>
                </div>
                <div class="form-group">
					<label for="eventDate">Fecha del evento</label>
                <input type="date" class="form-control" id="eventDate" name="eventDate" value="{{old('eventDate', $event->eventDate)}}" />
				</div>
                <div class="form-group">
                    <img src="{{asset('storage/event/'.$event->image)}}" alt="{{$event->image}}" width="200"  height="200" class="img-thumbnail">
					<input type="file" class="form-control-file" id="img" name="img">
                </div>
                <div class="form-group">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="executive" value="executive" name="userType[]"
                        {{ ((is_array( old('userType') ) && in_array('executive', old('userType')) ) || (!is_array( old('userType') ) && $event->executive ) )  ? 'checked' : '' }}
                            >
                        <label class="form-check-label" for="executive">Comisión directiva</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="patner" value="patner" name="userType[]"
                        {{ ( (is_array( old('userType') ) && in_array('patner', old('userType') ) ) || (!is_array( old('userType') ) && $event->patner ) )  ? 'checked' : '' }}>
                        <label class="form-check-label" for="patner">Socio</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" id="destacado" value="1" {{(!$errors->any() &&  $event->destacado) || ($errors->any() && old('destacado'))  ? 'checked' : '' }} name="destacado">
                        <label class="custom-control-label" for="destacado">Destacado</label>
                    </div>
                </div>
				<div class="form-group">
					<div class="custom-control custom-switch">
						<input type="checkbox" class="custom-control-input" id="notification" {{ $event->notification ? 'checked' : ''}} value="1" name="notification">
						<label class="custom-control-label" for="notification">Notificación</label>
					</div>
				</div>
				<button type="submit" class="btn btn-primary">Actualizar noticia</button>
			</form>
		</div>
	</div>
@endsection

