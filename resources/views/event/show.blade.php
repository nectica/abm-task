@extends('layouts.layout')
@section('title', $event->title)
@section('content')
<div class="row  m-3" >
    <div class="col-4 offset-1">
        <h2>{{$event->title}}</h2>
    </div>

</div>

<div class="row justify-content-center mb-5 px-2">
    <div class="col-8">
        <div class="text-center">
            <img src="{{ asset('storage/event/'.$event->image) }}" class="img-fluid" alt="{{$event->image}}">

        </div>
    </div>

</div>
<div class="row justify-content-center">
    <div class="col-8 ">
            <p class="lead">
                {{$event->details}}
            </p>
    </div>

</div>

@endsection

