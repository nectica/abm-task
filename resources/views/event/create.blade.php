@extends('form.index')
@section('title', 'Crear evento')
@section('title-form', 'Crear evento.')
@section('route-form', route('event.store'))
@section('content-form')
    <div class="form-group">
        <label for="title">Titulo</label>
        <input type="text" class="form-control" id="title" name="title" value="{{ old('title') }}">
    </div>
    <div class="form-group">
        <label for="description">Detalles</label>
        <textarea class="form-control" id="description" name="description" rows="3">{{ old('description') }}</textarea>
    </div>
    <div class="form-group">
        <label for="eventDate">Fecha del evento</label>
        <input type="date" class="form-control" id="eventDate" name="eventDate" value="{{ old('eventDate') }}" />
    </div>
    <div class="form-group">
        <label for="img">Imagen de eventos</label>
        <input type="file" class="form-control-file" id="img" name="img">
    </div>
    <div class="form-group">
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" aria-describedby='userType-feedback' id="executive" value="executive" name="userType[]"
                {{ is_array(old('userType')) && in_array('executive', old('userType')) ? 'checked' : '' }}>
            <label class="form-check-label" for="executive">Comisión directiva</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="patner" value="patner" name="userType[]"
                {{ is_array(old('userType')) && in_array('patner', old('userType')) ? 'checked' : '' }}>
            <label class="form-check-label" for="patner">Socio</label>
        </div>
        @error('userType')
            <div id="userType-feedback" class="is-invalid ">
                <small class="text-danger">*{{ $message }}</small>
            </div>
        @enderror
    </div>
    <div class="form-group">
        <div class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input" id="destacado" value="1" name="destacado">
            <label class="custom-control-label" for="destacado">Destacado</label>
        </div>
    </div>
	<div class="form-group">
        <div class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input" id="notification" {{ old('notification') ? 'checked' : ''}} value="1" name="notification">
            <label class="custom-control-label" for="notification">Notificación</label>
        </div>
    </div>
    <button type="submit" class="btn btn-success-caem">Crear evento</button>

@endsection
