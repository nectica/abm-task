@extends('layouts.layout')
@section('title', 'Pagina principal de minerales')
@section('content')
    <div class="row justify-content-center">
        @if ($message = Session::get('success'))
            <div class=" col-6  mt-2 alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('error'))
            <div class=" col-6  mt-2 alert alert-error">
                <p>{{ $message }}</p>
            </div>
        @endif

    </div>
    <div class="row justify-content-between m-3">
        <div class="col-lg-3 offset-1">
            <h2 class="font-color-caem">Precio de minerales</h2>
        </div>
        <div class="col-lg-3 offset-3">
            <form action="{{ route('mineralesValor.filter') }}" method="POST">
                @csrf
                <div class="input-group ">
                    <input type="text" class="form-control" id="searchInput" name="title"
                        placeholder="Filtrar por nombre o descripción">
                    <div class="input-group-append">
                        <button class="btn btn-primary " type="submit">Filtrar</button>
                    </div>
                </div>
            </form>
        </div>
        @can('mineral-create')
            <div class="col-lg-2">
                <a href="{{ route('mineralesValor.create') }}" type="button" class="btn btn-success-caem">Crear valor de mineral</a>
            </div>
        @endcan
    </div>
    <div class="row justify-content-center">
        <div class="col-10">
            <table class="table table-bordered">
                <thead>
                    <th scope="col">@sortablelink('mineral_id', 'Mineral')</th>
                    <th scope="col">@sortablelink('ano', 'Año') </th>
                    <th scope="col">@sortablelink('mes','Mes') </th>
                    <th scope="col">@sortablelink('valor', 'Precio') </th>
                    <th scope="col">@sortablelink('exportacion', 'Exportaciones') </th>
                    <th scope="col"></th>
                </thead>
                <tbody>
                    @foreach ($valores as $valor)
                        <tr>
                            <td>{{ $valor->mineral->name }}</td>
                            <td>{{  $valor->ano }}</td>
                            <td>{{  $meses[$valor->mes]  }}</td>
                            <td>{{ number_format($valor->valor, 0, ',', '.') }}</td>
                            <td>{{ number_format($valor->exportacion, 0, ',', '.')  }}</td>
                            <td>
                                @can('mineral-edit')
                                    <a href="{{ route('mineralesValor.edit', $valor->id) }}" type="button"
                                        class="btn btn-outline-primary  btn-sm  my-1"><i class="fas fa-edit"></i></a>
                                @endcan
                                @can('mineral-delete')
                                    <form name="form-delete" action="{{ route('mineralesValor.delete', $valor->id) }}"
                                        method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-outline-danger btn-sm" disabled><i class="fas fa-trash-alt"></i></button>
                                    </form>
                                @endcan

                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $valores->appends(\Request::except('page'))->render() }}
        </div>

    </div>


@endsection
<script src="{{ asset('js/delete.js') }}" defer></script>
