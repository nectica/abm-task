@extends('form.index')
@section('title', 'Actualizar precio para mineral')
@section('title-form', 'Actualizar precio para mineral.')
@section('route-form', route('mineralesValor.update', $mineralValor->id))
@section('content-form')
    @method('PUT')
	<input type="hidden" name="url" value="{{ old('url', url()->previous()) }}" />
    <div class="form-group row col-sm-5">
        <label for="mes">Mes</label>
        <select class="form-control" name="mes" id="mes" required>
            <option value="" disabled selected>-- Seleccione un Mes --</option>
            @for ($i = 0; $i < count($meses); $i++)
                <option value="{{ $i  }}" {{ old('mes', $mineralValor->mes ) == $i ? 'selected' : '' }}>{{ $meses[$i] }}</option>
            @endfor
        </select>
        @error('mes')
            <div id="details-feedback" class="is-invalid ">
                <small class="text-danger">*{{ $message }}</small>
            </div>
        @enderror
    </div>
    <div class="form-group row col-sm-5">
        <label for="ano">Año</label>
        <select class="form-control" name="ano" id="ano" required>
            <option value="" disabled selected>-- Seleccione un Año --</option>
            @for ($i = $anio - 5; $i < $anio + 5; $i++)
                <option value="{{ $i }}" {{ old('ano', $mineralValor->ano ) == $i ? 'selected' : '' }}>{{ $i }}</option>

            @endfor

        </select>
        @error('ano')
            <div id="details-feedback" class="is-invalid ">
                <small class="text-danger">*{{ $message }}</small>
            </div>
        @enderror
    </div>
    <div class="form-row mb-2">
        <div class="col-md-3">
            <label for="mineral_id">Mineral</label>
            <input type="hidden" name="mineral_id" value="{{ $mineral->id }}">
            <select class="form-control" id="mineral_id" disabled>
                <option selected>{{ $mineral->name }}</option>
            </select>
        </div>
        <div class="col-md-3">
            <label for="valor">Precio</label>
            <input type="text" class="form-control" name="valor" id="valor" value="{{ old('valor', $mineralValor->valor) }}">

        </div>
        <div class="col-md-3">
            <label for="exportacion">Exportaciones</label>
            <input type="text" class="form-control" name="exportacion" id="exportacion" value="{{ old('exportacion', $mineralValor->exportacion)}}" >

        </div>
    </div>
{{--
    @foreach ($minerales as $mineral)
    @endforeach --}}

    <button type="submit" class="btn btn-primary">Actualizar valor de mineral </button>
@endsection
