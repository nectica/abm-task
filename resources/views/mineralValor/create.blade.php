@extends('form.index')
@section('title', 'Crear valor para mineral')
@section('title-form', 'Crear valor para mineral.')
@section('route-form', route('mineralesValor.store'))
@section('content-form')
    <div class="form-group row col-sm-5">
        <label for="mes">Mes</label>
        <select class="form-control" name="mes" id="mes" required>
            <option value="" disabled selected>-- Seleccione un Mes --</option>
            @for ($i = 0; $i < count($meses); $i++)
                <option value="{{ $i  }}" {{ old('mes') == $i  ? 'selected' : '' }}>{{ $meses[$i] }}</option>

            @endfor

        </select>
        @error('mes')
            <div id="details-feedback" class="is-invalid ">
                <small class="text-danger">*{{ $message }}</small>
            </div>
        @enderror
    </div>
    <div class="form-group row col-sm-5">
        <label for="ano">Año</label>
        <select class="form-control" name="ano" id="ano" required>
            <option value="" disabled selected>-- Seleccione un Año --</option>
            @for ($i = $anio - 5; $i < $anio + 5; $i++)
                <option value="{{ $i }}" {{ old('ano') == $i ? 'selected' : '' }}>{{ $i }}</option>

            @endfor

        </select>
        @error('ano')
            <div id="details-feedback" class="is-invalid ">
                <small class="text-danger">*{{ $message }}</small>
            </div>
        @enderror
    </div>

    @foreach ($minerales as $key => $mineral)
        <div class="form-row mb-2">
            <div class="col-md-3">
                <label for="mineral_id">Mineral</label>
                <input type="hidden" name="mineral_id[]" value="{{ $mineral->id }}">
                <select class="form-control" id="mineral_id" disabled>
                    <option selected>{{ $mineral->name  }}</option>
                </select>
            </div>
            <div class="col-md-3">
                <label for="valor">Precio</label>
                <input type="text" class="form-control" name="valor[]" id="valor" value="{{ old('valor.'.$key) }}">
                @error('valor.'.$key)
                    <div id="details-feedback" class="is-invalid ">
                        <small class="text-danger">*{{ $message }}</small>
                    </div>
                @enderror
            </div>
            <div class="col-md-3">
                <label for="exportacion">Exportaciones</label>
                <input type="text" class="form-control" name="exportacion[]" id="exportacion"  value="{{ old('exportacion.'.$key) }}">
                @error('exportacion.'.$key)
                <div id="details-feedback" class="is-invalid ">
                    <small class="text-danger">*{{ $message }}</small>
                </div>
            @enderror
            </div>

        </div>
    @endforeach

    <button type="submit" class="btn btn-success-caem">Registrar valor de mineral </button>
@endsection
