@extends('layouts.layout')
@section('title', $news->title)
@section('content')
<div class="row  m-3" >
    <div class="col-4 offset-1">
        <h2>{{$news->title}}</h2>
    </div>

</div>
<div class="row justify-content-center mb-5 px-2">
    <div class="col-8">
        <div class="text-center">
            <img src="{{ asset('storage/news/'.$news->image) }}" class="img-fluid" alt="{{$news->image}}">

        </div>
    </div>

</div>
<div class="row justify-content-center">
    <div class="col-8 ">
            <p class="lead">
                {{$news->details}}
            </p>
    </div>

</div>

@endsection

