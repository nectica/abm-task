@extends('layouts.layout')
@section('title', 'Pagina principal')
@section('content')
<div class="row justify-content-between m-3">
    <div class="col-lg-3 offset-1">
        <h2 class="font-color-caem">Pagina Principal</h2>
    </div>

</div>
<div class="row  m-3">
    <div class=" card col-lg-2 offset-1 mb-2">
        <div class="card-body">
            <h5 class="card-title font-color-caem">Numero de Consultas creadas</h5>
            <h6 class="card-subtitle mb-2 text-muted">en los últimos 7 días</h6>
            <h1 class="card-text text-center mt-4"><a class="font-color-caem" href="{{ $queries > 0 ? route('home.query') : '#' }}">{{$queries}}</a></h1>

          </div>
    </div>
    <div class=" card col-lg-2 offset-1 mb-2">
        <div class="card-body">
            <h5 class="card-title font-color-caem">Numero de usuarios registrados</h5>
            <h6 class="card-subtitle mb-2 text-muted">en los últimos 7 días</h6>
            <h1 class="card-text text-center mt-4"><a class="font-color-caem" href="{{ $users > 0 ? route('home.user'): '#' }}">{{$users}}</a> </h1>
          </div>
    </div>
    <div class=" card col-lg-2 offset-1 mb-2  ">
        <div class="card-body">
            <h5 class="card-title font-color-caem">Reportes registrados en los últimos </h5>
            <h6 class="card-subtitle mb-2 text-muted">en los últimos 7 días</h6>
            <h1 class="card-text text-center mt-4"><a class="font-color-caem" href="{{ $reports > 0 ? route('home.report') : '#' }}">{{$reports}}</a> </h1>
          </div>
    </div>
    <div class=" card col-lg-2 offset-1 mb-2">
        <div class="card-body">
            <h5 class="card-title font-color-caem">Numero de eventos </h5>
            <h6 class="card-subtitle mb-2 text-muted">en los próximos 7 días</h6>
            <h1 class="card-text text-center mt-4"><a class="font-color-caem" href="{{ $events > 0 ? route('home.event'): '#' }}">{{$events}}</a></h1>
          </div>
    </div>
    <div class=" card col-lg-2 offset-1 mb-2">
        <div class="card-body">
            <h5 class="card-title font-color-caem">Google Play</h5>
            <h6 class="card-subtitle mb-2 text-muted">Enlance de la App Android</h6>
            <h1 class="card-text text-center mt-4"></h1>
            <a href="#" class="card-link">link</a>

          </div>
    </div>
    <div class=" card col-lg-2 offset-1 mb-2">
        <div class="card-body">
            <h5 class="card-title font-color-caem">App Store</h5>
            <h6 class="card-subtitle mb-2 text-muted">Enlance de la App IOS</h6>
            <h1 class="card-text text-center mt-4"></h1>
            <a href="#" class="card-link">link</a>

          </div>
    </div>
</div>

@endsection
