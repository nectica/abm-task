<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <p>Hola {{$details['name'] }}, ha recibido este email para la recuperación de contraseña,
    si desea continuar  haga clic en el siguiente botón</p>
    <a href="{{ route('userAbm.recoveryPassword',['id' => $details['id'],  'token' => $details['token']])}}" class="btn btn-success-caem">Restablecer contraseña</a>
</body>
</html>

