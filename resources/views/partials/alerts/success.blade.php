<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contraseña actualizada</title>
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div class="container-fluid p-0">
        <div class="row justify-content-center">
            <div class="col-8 mt-5">
                <div class="alert alert-success">
                  <p>{{$success}}</p>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
