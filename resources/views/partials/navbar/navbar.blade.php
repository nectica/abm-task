
<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container">
        <a class="navbar-brand logo" href="{{ url('/') }}">
            <img src="{{ asset('storage/images/Logo_CAEM.png')}}" height="45" alt="logo">
            Gestor de contenidos
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    @include('partials.navbar.guest.index')
                @else
                    @role('Admin')
                        @include('partials.navbar.host.admin')
                    @endrole
                    @include('partials.navbar.host.abm')
                @endguest
            </ul>
        </div>
    </div>
</nav>
