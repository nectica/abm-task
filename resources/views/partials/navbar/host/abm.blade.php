<li class="nav-item dropdown">
    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
        Abm Menu
    </a>

    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

        @can('news-list')
            <a class="dropdown-item" href="{{ route('news.index') }}">Ver Noticias</a>
        @endcan
        @can('event-list')
            <a class="dropdown-item" href="{{ route('event.index') }}">Ver Eventos</a>
        @endcan
        @can('news-list')
            <a class="dropdown-item" href="{{ route('userAbm.index') }}">Ver Usuarios</a>
        @endcan
        @can('news-list')
            <a class="dropdown-item" href="{{ route('report.index') }}">Ver reportes</a>
        @endcan
        @can('news-list')
            <a class="dropdown-item" href="{{ route('query.index') }}">Ver consultas</a>
        @endcan
        @can('news-list')
            <a class="dropdown-item" href="{{ route('mineralesValor.index') }}">Ver Minerales</a>
        @endcan
		@can('status-contact-list')
			<a class="dropdown-item" href="{{ route('status.index') }}">Estados de Consultas</a>
		@endcan

       {{--  @can('news-create')
            <a class="dropdown-item" href="{{ route('news.create') }}">Crear Noticia</a>
        @endcan --}}
        {{-- @can('event-create')
            <a class="dropdown-item" href="{{ route('event.create') }}">Crear Eventos</a>
        @endcan --}}
    </div>
</li>
