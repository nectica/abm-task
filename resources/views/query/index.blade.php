@extends('layouts.layout')
@section('title', 'Pagina principal')
@section('content')
<div class="row justify-content-center">
    @if ($message = Session::get('success'))
        <div class=" col-6  mt-2 alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
</div>
<div class="row justify-content-around m-3" >
    <div class="col-lg-3 ">
        <h2>Consultas</h2>
    </div>
    <div class="col-lg-2">
        <form action="{{ route('query.filter') }}" method="POST">
            @csrf
            <div class="input-group ">
                <input type="text" class="form-control" id="searchInput" name="title"
                    placeholder="Filtrar por nombre o descripción">
                <div class="input-group-append">
                    <button class="btn btn-primary " type="submit">Filtrar</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="row justify-content-center">
	<div class="col-10 table-responsive">
		<table class="table table-bordered">
			<thead>
				<th scope="col">@sortablelink('name', 'Nombre')</th>
				<th scope="col">@sortablelink('topic', 'Tema numerico') </th>
				<th scope="col">@sortablelink('query', 'Consulta') </th>
				<th scope="col">@sortablelink('company', 'Compañia') </th>
                <th scope="col">@sortablelink('phone','Telefono') </th>
                <th scope="col">@sortablelink('created_at','Fecha de creación') </th>
                <th scope="col">@sortablelink('status_contacts_id','Estado') </th>
				<th scope="col"></th>
			</thead>
			@foreach ($contacts as $contact)
				<tbody>
					<tr>
						<td><a href="{{route('query.show', $contact->id )}}" >{{$contact->name}}</a></td>
						<td>{{$contact->topic}}</td>
						<td>{{ strlen($contact->query) > 255  ? substr($contact->query,0 , 255).'...' : $contact->query}}</td>
                        <td>{{$contact->company}}</td>
                        <td>{{$contact->phone}}</td>
                        <td>{{date_format( $contact->created_at,'d/m/Y')   }}</td>
						<td>{{ isset($contact->status) ? $contact->status->name : '-' }}</td>
						<td>
							@can('report-edit')
								<a href="{{ route('query.edit', $contact->id) }}" type="button"
									class="btn btn-outline-primary btn-sm  my-1"> <i class="fas fa-edit"></i></a>
							@endcan
						</td>
					</tr>
				</tbody>
			@endforeach
		</table>
		{{$contacts->appends(\Request::except('page'))->render()}}
	</div>

</div>


@endsection

