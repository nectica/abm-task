@extends('form.index')
@section('title', 'Crear Noticia')
@section('title-form', 'Crear Noticia.')
@section('route-form', route('news.store'))
@section('content-form')
    <div class="form-group">
        <label for="title">Titulo</label>
        <input type="text" class="form-control" id="title" name="title" aria-describedby='title-feedback'
            value="{{ old('title') }}">
        @error('title')
            <div id="title-feedback" class="is-invalid ">
                <small class="text-danger">*{{ $message }}</small>
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="description">Detalles</label>
        <textarea class="form-control" id="description" name="description" aria-describedby='details-feedback'
            rows="3">{{ old('description') }}</textarea>
        @error('description')
            <div id="details-feedback" class="is-invalid ">
                <small class="text-danger">*{{ $message }}</small>
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="img">Imagen para la noticia</label>
        <input type="file" class="form-control-file" id="img" name="img">
        @error('img')
            <div id="title-feedback" class="is-invalid ">
                <small class="text-danger">*{{ $message }}</small>
            </div>
        @enderror
    </div>
    <div class="form-group">
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" aria-describedby='userType-feedback' id="executive" value="executive" name="userType[]"
                {{ is_array(old('userType')) && in_array('executive', old('userType')) ? 'checked' : '' }}>
            <label class="form-check-label" for="executive">Comisión directiva</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="patner" value="patner" name="userType[]"
                {{ is_array(old('userType')) && in_array('patner', old('userType')) ? 'checked' : '' }}>
            <label class="form-check-label" for="patner">Socio</label>
        </div>
        @error('userType')
            <div id="userType-feedback" class="is-invalid ">
                <small class="text-danger">*{{ $message }}</small>
            </div>
        @enderror
    </div>
    <div class="form-group">
        <div class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input" id="destacado" value="1" name="destacado">
            <label class="custom-control-label" for="destacado">Destacado</label>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Crear noticia</button>
@endsection
