@extends('layouts.layout')
@section('title', $contact->name)
@section('content')
    <div class="row  m-3">
        <div class="col-4 offset-1">
            <h2>{{ $contact->name }}</h2>
        </div>

    </div>
    <div class="row justify-content-center mb-5 px-2">
        <div class="col-12">
            <div class="text-center">
               <h3>Tema numerico: {{$contact->topic}}</h3>
            </div>
        </div>
        <div class="col-4">
            <ul>
                <li>Compañia: {{$contact->company}} </li>
                <li>Telefono: {{$contact->phone}} </li>
                <li>Usuario: {{ isset($contact->userAbm) ? $contact->userAbm->name .' - '.$contact->userAbm->email : 'No tiene usuario asignado.'}} </li>
				<li>Estado: {{ isset($contact->status) ? $contact->status->name : '-' }}</li>
            </ul>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-8 ">
            <p class="lead">
                {{ $contact->query }}
            </p>
        </div>

    </div>

@endsection
