@extends('form.index')
@section('title', 'Actualizar Noticia')
@section('title-form', 'Actualizar Noticia.')
@section('route-form', route('query.update', $query->id) )
@section('content-form')
	@method('PUT')
	<input type="hidden" name="url" value="{{ old('url', url()->previous()) }}" />
	<input type="hidden" name="id" value="{{ $query->id }}">
	<div class="form-group">
		<label for="name">Nombre</label>
		<input type="text" class="form-control" disabled id="name" name="name" aria-describedby='title-feedback' value="{{ $query->name  }}">
		@error('name')
			<div id="name-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>
	<div class="form-group">
		<label for="name">Compañia</label>
		<input type="text" class="form-control" disabled id="company" name="company" aria-describedby='company-feedback' value="{{ $query->company  }}">
		@error('company')
			<div id="company-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>
	<div class="form-group">
		<label for="name">Telefono</label>
		<input type="text" class="form-control" disabled id="phone" name="phone" aria-describedby='phone-feedback' value="{{ $query->phone  }}">
		@error('phone')
			<div id="phone-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>
	<div class="form-group">
		<label for="name">Tema</label>
		<input type="text" class="form-control" disabled id="topic" name="topic" aria-describedby='topic-feedback' value="{{ $query->topic  }}">
		@error('topic')
			<div id="phone-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>
    <div class="form-group">
        <label for="query">Consulta</label>
        <textarea disabled class="form-control" id="query" name="query" aria-describedby='query-feedback' rows="3">{{ $query->query }}</textarea>
        @error('query')
			<div id="query-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
    </div>
	<div class="form-group col-lg-4 pl-0">
		<label for="status_contacts_id">Estado</label>
		<select class="form-control" name="status_contacts_id" id="status_contacts_id">
			<option selected disabled>Seleccionar un estado.</option>
			@foreach ($statuses as $status)
				<option {{$query->status_contacts_id == $status->id ? 'selected' : ''}} value="{{$status->id}}">{{$status->name}} </option>
			@endforeach
		</select>
	</div>
	<button type="submit" class="btn btn-primary">Actualizar noticia</button>
@endsection
