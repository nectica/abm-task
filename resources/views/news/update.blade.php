@extends('form.index')
@section('title', 'Actualizar Noticia')
@section('title-form', 'Actualizar Noticia.')
@section('route-form', route('news.update'))
@section('content-form')
    @method('PUT')
    <input type="hidden" name="id" value="{{ $news->id }}">
	<input type="hidden" name="url" value="{{ old('url', url()->previous()) }}" />

    <div class="form-group">
        <label for="title">Titulo</label>
        <input type="text" class="form-control" id="title" name="title" aria-describedby='title-feedback' value="{{ old('title', $news->title)  }}">
        @error('title')
			<div id="title-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
    </div>
    <div class="form-group">
        <label for="description">Detalles</label>
        <textarea class="form-control" id="description" name="description" aria-describedby='details-feedback' rows="3">{{ old('description', $news->details) }}</textarea>
        @error('description')
			<div id="details-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
    </div>
    <div class="form-group">
        <img src="{{ asset('storage/news/' . $news->image) }}" alt="{{ $news->image }}" width="200" height="200"
            class="img-thumbnail">
        <input type="file" class="form-control-file" id="img" name="img" aria-describedby='img-feedback'>
         @error('img')
			<div id="details-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
    </div>
    <div class="form-group">
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="executive" value="executive" name="userType[]"
            {{ ((is_array( old('userType') ) && in_array('executive', old('userType')) ) || (!is_array( old('userType') ) && $news->executive ) )  ? 'checked' : '' }}
                >
            <label class="form-check-label" for="executive">Comisión directiva</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="patner" value="patner" name="userType[]"
            {{ ( (is_array( old('userType') ) && in_array('patner', old('userType') ) ) || (!is_array( old('userType') ) && $news->patner ) )  ? 'checked' : '' }}>
            <label class="form-check-label" for="patner">Socio</label>
        </div>
    </div>
    <div class="form-group">
        <div class="custom-control custom-switch">
        <input type="checkbox" class="custom-control-input" id="destacado" value="1" {{(!$errors->any() &&  $news->destacado) || ($errors->any() && old('destacado'))  ? 'checked' : '' }} name="destacado">
            <label class="custom-control-label" for="destacado">Destacado</label>
        </div>
    </div>
	<div class="form-group">
        <div class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input" id="notification" {{ $news->notification ? 'checked' : ''}} value="1" name="notification">
            <label class="custom-control-label" for="notification">Notificación</label>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Actualizar noticia</button>
@endsection
