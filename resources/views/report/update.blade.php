@extends('form.index')
@section('title', 'Actualizar reporte')
@section('title-form', 'Actualizar Reporte.')
@section('route-form', route('report.update', $report->id))
@section('content-form')
    @method('PUT')
	<input type="hidden" name="url" value="{{ old('url', url()->previous()) }}" />
    <input type="hidden" name="id" value="{{ $report->id }}">
    <div class="form-group">
        <label for="title">Titulo</label>
        <input type="text" class="form-control" id="title" name="title" aria-describedby='title-feedback' value="{{ old('title', $report->title)  }}">
        @error('title')
			<div id="title-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
    </div>
    <div class="form-group">
        <label for="copete">Copete</label>
        <textarea class="form-control" id="copete" name="copete" aria-describedby='details-feedback' rows="3">{{ old('copete', $report->copete) }}</textarea>
        @error('copete')
			<div id="details-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
    </div>
    <div class="form-group">
        <span>Reporte: {{ $report->report}}</span>
        <input type="file" class="form-control-file" id="reporte" name="reporte" aria-describedby='reporte-feedback'>
         @error('reporte-feedback')
			<div id="details-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
    </div>
    <div class="form-group">
        <img src="{{ asset('storage/reports/' . $report->image) }}" alt="{{ $report->image }}" width="200" height="200"
            class="img-thumbnail">
        <input type="file" class="form-control-file" id="image" name="image" aria-describedby='img-feedback'>
         @error('image')
			<div id="details-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
    </div>
    <div class="form-group">
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="executive" value="executive" name="userType[]"
            {{ ((is_array( old('userType') ) && in_array('executive', old('userType')) ) || (!is_array( old('userType') ) && $report->executive ) )  ? 'checked' : '' }}
                >
            <label class="form-check-label" for="executive">Comisión directiva</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="patner" value="patner" name="userType[]"
            {{ ( (is_array( old('userType') ) && in_array('patner', old('userType') ) ) || (!is_array( old('userType') ) && $report->patner ) )  ? 'checked' : '' }}>
            <label class="form-check-label" for="patner">Socio</label>
        </div>
    </div>

    <button type="submit" class="btn btn-primary">Actualizar Reporte</button>
@endsection
