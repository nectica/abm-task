@extends('layouts.layout')
@section('title', 'Pagina principal de reporte')
@section('content')
    <div class="row justify-content-center">
        @if ($message = Session::get('success'))
            <div class=" col-6  mt-2 alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
    </div>
    <div class="row justify-content-between m-3">
        <div class="col-lg-3 offset-1">
            <h2 class="font-color-caem">Reportes</h2>
        </div>

        <div class="col-lg-3 offset-3">
            <form action="{{ route('report.filter') }}" method="POST">
                @csrf
                <div class="input-group ">
                    <input type="text" class="form-control" id="searchInput" name="title"
                        placeholder="Filtrar por nombre o descripción">
                    <div class="input-group-append">
                        <button class="btn btn-primary " type="submit">Filtrar</button>
                    </div>
                </div>
            </form>
        </div>
        @can('report-create')
            <div class="col-lg-2">
                <a href="{{ route('report.create') }}" type="button" class="btn btn-success-caem">Crear Reporte</a>
            </div>
        @endcan
    </div>
    <div class="row justify-content-center">
        <div class="col-10 table-responsive">
            <table class="table table-bordered">
                <thead>
                    <th scope="col">@sortablelink('title', 'Titulo')</th>
                    <th scope="col">@sortablelink('copete', 'copete') </th>
                    <th scope="col">@sortablelink('fileName','Nombre de archivo') </th>
                    <th scope="col">@sortablelink('report','reporte') </th>
                    <th scope="col">@sortablelink('image', 'Imagen') </th>
                    <th scope="col">@sortablelink('executive', 'Comisión Directiva') </th>
                    <th scope="col">@sortablelink('patner', 'Socio') </th>
                    <th scope="col"></th>
                </thead>
                @foreach ($reports as $report)
                    <tbody>
                        <tr>
                            <td><a href="{{ route('report.show', $report->id) }}">{{ $report->title }}</a></td>
                            <td>{{ strlen($report->copete) > 255 ? substr($report->copete, 0, 255) . '...' : $report->copete }}
                            </td>
                            <td>{{ $report->fileName }}</td>
                            <td>{{ $report->report }}</td>
                            <td>  <img src="{{ asset('storage/reports/' . $report->image  ) }}" alt="{{ $report->image }}" width="50" height="50"
                                class="img-thumbnail"/></td>
                            <td>{{ $report->executive == 1 ? 'Si' : 'No' }}</td>
                            <td>{{ $report->patner == 1 ? 'Si' : 'No' }}</td>
                            <td>
                                @can('report-edit')
                                    <a href="{{ route('report.edit', $report->id) }}" type="button"
                                        class="btn btn-outline-primary  btn-sm  my-1"><i class="fas fa-edit"></i></a>
                                @endcan
                                @can('report-delete')
                                    <form name="form-delete" action="{{ route('report.delete', $report->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-outline-danger btn-sm" disabled><i class="fas fa-trash-alt"></i></button>
                                    </form>
                                @endcan

                            </td>
                        </tr>
                    </tbody>
                @endforeach
            </table>
            {{ $reports->appends(\Request::except('page'))->render() }}
        </div>

    </div>


@endsection
<script src="{{ asset('js/delete.js') }}" defer></script>
