@extends('layouts.layout')
@section('title', $report->title)
@section('content')
<div class="row  m-3" >
    <div class="col-4 offset-1">
        <h2>{{$report->title}}</h2>
    </div>

</div>
<div class="row justify-content-center mb-5 px-2">
    <div class="col-8">
        <p class="lead">
         Copete: {{$report->copete}}
        </p>
    </div>
    <div class="col-8">
        <p class="lead">
            Tipo de usuario:
            {{ $report->patner ? 'Es socio' : '' }}
            {{ $report->patner && $report->executive ? ' y ' : '' }}
            {{ $report->executive ? 'Es comision directiva' : '' }}
            {{ !$report->patner && !$report->executive ? 'Ninguno' : '.' }}

        </p>
    </div>
</div>
<div class="row justify-content-center mb-5 px-2">
    <img src="{{ asset('storage/reports/' . $report->image) }}" alt="{{ $report->image }}" width="200" height="200"
                class="img-thumbnail">
</div>
<div class="row justify-content-center mb-5 px-2">
    <div class="col-8">
    <a href="{{route('report.download', $report->id)}}">{{$report->fileName}}</a>
    </div>
</div>

@endsection

