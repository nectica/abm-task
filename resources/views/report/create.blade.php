@extends('form.index')
@section('title', 'Crear Reporte')
@section('title-form', 'Crear Reporte.')
@section('route-form', route('report.store'))
@section('content-form')
    <div class="form-group">
        <label for="title">Titulo</label>
        <input type="text" class="form-control" id="title" name="title" aria-describedby='title-feedback'
            value="{{ old('title') }}">
        @error('title')
            <div id="title-feedback" class="is-invalid ">
                <small class="text-danger">*{{ $message }}</small>
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="copete">Copete</label>
        <input type="text" class="form-control" id="copete" name="copete" aria-describedby='copete-feedback' value="{{ old('copete') }}"
            />
        @error('copete')
            <div id="copete-feedback" class="is-invalid ">
                <small class="text-danger">*{{ $message }}</small>
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="reporte">Reporte</label>
        <input type="file" class="form-control-file" id="reporte" name="reporte">
        @error('reporte')
            <div id="title-feedback" class="is-invalid ">
                <small class="text-danger">*{{ $message }}</small>
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="image">Imagen</label>
        <input type="file" class="form-control-file" id="image" name="image">
        @error('image')
            <div id="title-feedback" class="is-invalid ">
                <small class="text-danger">*{{ $message }}</small>
            </div>
        @enderror
    </div>
    <div class="form-group">
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" aria-describedby='userType-feedback' id="executive" value="executive" name="userType[]"
                {{ is_array(old('userType')) && in_array('executive', old('userType')) ? 'checked' : '' }}>
            <label class="form-check-label" for="executive">Comisión directiva</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="patner" value="patner" name="userType[]"
                {{ is_array(old('userType')) && in_array('patner', old('userType')) ? 'checked' : '' }}>
            <label class="form-check-label" for="patner">Socio</label>
        </div>
        @error('userType')
            <div id="userType-feedback" class="is-invalid ">
                <small class="text-danger">*{{ $message }}</small>
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-success-caem">Crear Reporte</button>
@endsection
