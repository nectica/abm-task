@extends('form.index')
@section('title', 'Crear Estado.')
@section('title-form', 'Crear estado.')
@section('route-form', route('status.store'))
@section('content-form')
	<div class="form-group col-lg-4 pl-0">
		<label for="title">Nombre</label>
		<input type="text" class="form-control" id="name" name="name" aria-describedby='title-feedback'
			value="{{ old('name') }}">
		@error('name')
			<div id="title-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>

	<button type="submit" class="btn btn-success mb-1">Crear estado</button>
@endsection
