@extends('form.index')
@section('title', 'Actualizar estado.')
@section('title-form', 'Actualizar estado.')
@section('route-form', route('status.update',$statusContact->id))
@section('content-form')
@method('PUT')
<input type="hidden" name="url" value="{{ old('url', url()->previous()) }}" />

	<div class="form-group col-lg-4 pl-0">
		<label for="title">Nombre</label>
		<input type="text" class="form-control" id="name" name="name" aria-describedby='title-feedback'
			value="{{ old('name', $statusContact->name) }}">
		@error('name')
			<div id="title-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>

	<button type="submit" class="btn btn-success mb-1">Actualizar estado</button>
@endsection
