@extends('layouts.layout')
@section('title', 'Pagina de estados')
@section('content')
	<div class="row justify-content-center">
		@if ($message = Session::get('success'))
			<div class=" col-6  mt-2 alert alert-success">
				<p>{{ $message }}</p>
			</div>
		@endif

	</div>
	<div class="row justify-content-between m-3">
		<div class="col-lg-3 offset-1">
			<h2 class="font-color-gergal">Estados</h2>
		</div>
		<div class="col-lg-3 offset-3">
			{{-- <form action="{{ route('estados.filter') }}" method="POST">
				@csrf
				<div class="input-group ">
					<input type="text" class="form-control"  name="nombre"
						placeholder="Filtrar por nombre">
					<div class="input-group-append">
						<button class="btn btn-gray " type="submit">Filtrar</button>
					</div>
				</div>
			</form> --}}
		</div>
		@can('status-contact-create')
			<div class="col-lg-2">
				<a href="{{ route('status.create') }}" type="button" class="btn btn-success-caem">Crear Estado</a>
			</div>
		@endcan
	</div>
	<div class="row justify-content-center">
		<div class="col-12 col-xl-10 table-responsive">
            @if ($estados->isEmpty())
                <p class="lead">No existen Estados</p>
            @else
                <table class="table table-bordered">
                    <thead>
                        @if ( !method_exists ($estados, 'render') )
                        <th>Nombre</th>
                        @else
                            <th scope="col">@sortablelink('descripcion', 'Nombre')</th>
                        @endif
                        <th scope="col"></th>
                    </thead>
                    <tbody>
                        @foreach ($estados as $estado)
                            <tr>
                                <td class="text-gergal-color">{{ $estado->name }}</td>

                                <td>
                                    @can('status-contact-edit')
                                        <a href="{{ route('status.edit', $estado->id) }}" type="button"
                                            class="btn btn-outline-primary btn-sm  my-1"> <i class="fas fa-edit"></i></a>
                                    @endcan
                                    @can('status-contact-delete')
                                        <form name="form-delete" action="{{ route('status.delete', $estado->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-outline-danger btn-sm" disabled><i class="fas fa-trash-alt"></i></button>
                                        </form>
                                    @endcan

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @endif
            @if( method_exists($estados, 'render'))
                {{ $estados->render() }}
            @endif
		</div>

	</div>


@endsection
<script src="{{ asset('js/delete.js') }}" defer></script>
