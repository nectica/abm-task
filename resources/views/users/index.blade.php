@extends('layouts.layout')
@section('content')

<div class="row justify-content-between my-3">
    <div class="col-lg-2 offset-lg-1">
        <h2 class="font-color-caem">Usarios</h2>
    </div>

    <div class="col-lg-2 " style="padding: 0">
        <a class="btn btn-success-caem" href="{{ route('users.create') }}"> Create nuevo usuario</a>
    </div>

</div>

@if ($message = Session::get('success'))

<div class="alert alert-success">
  <p>{{ $message }}</p>
</div>
@endif
@if ($errors->any())
	<div class="col">
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	</div>
@endif
<div class="row justify-content-center">
    <div class="col-10 table-responsive ">

        <table class="table  table-bordered">
         <thead>
           <th>No</th>
           <th >Nombre</th>
           <th >Email</th>
           <th>Rol</th>
           <th>Acción</th>
         </thead>
         @foreach ($data as $key => $user)
          <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>
              @if(!empty($user->getRoleNames()))
                @foreach($user->getRoleNames() as $v)
                   <label class="badge badge-success">{{ $v }}</label>
                @endforeach
              @endif
            </td>
            <td>
               <a class="btn btn-outline-success" href="{{ route('users.show', Crypt::encrypt($user->id)) }}"><i class="far fa-eye"></i></a>
               <a href="{{ route('users.edit', Crypt::encrypt($user->id)) }}" type="button"
                class="btn btn-outline-primary  btn-sm  my-1"><i class="fas fa-edit"></i></a>
               @if (Auth::user()->id != $user->id )

               @endif
               {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline', 'name' => 'form-delete']) !!}
                    <button type="submit" class="btn btn-outline-danger btn-sm" disabled><i class="fas fa-trash-alt"></i></button>
               {!! Form::close() !!}
            </td>
          </tr>
         @endforeach
        </table>
    </div>


    {!! $data->render() !!}
</div>

@endsection
<script src="{{ asset('js/delete.js') }}" defer></script>
