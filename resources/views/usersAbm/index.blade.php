@extends('layouts.layout')
@section('title', 'Pagina principal')
@section('content')
    <div class="row justify-content-center">
        @if ($message = Session::get('success'))
            <div class=" col-6  mt-2 alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
    </div>
    <div class="row justify-content-between m-3">
        <div class="col-lg-3 offset-1">
            <h2 class="font-color-caem">Usuarios de Abm</h2>
        </div>

        <div class="col-lg-3  offset-3">
            <form action="{{ route('userAbm.filter') }}" method="POST">
                @csrf
                <div class="input-group ">
                    <input type="text" class="form-control" id="searchInput" name="name"
                        placeholder="Filtrar por nombre o email">
                    <div class="input-group-append">
                        <button class="btn btn-primary " type="submit">Filtrar</button>
                    </div>
                </div>
            </form>
        </div>
        @can('news-create')
            <div class="col-lg-2">
                <a href="{{ route('userAbm.create') }}" type="button" class="btn btn-success-caem">Crear Usuario</a>
            </div>
        @endcan
    </div>



    <div class="row justify-content-center">
        <div class="col-10 table-responsive">
            <table id="myTable" class="table table-bordered">
                <thead>
                    <th scope="col">@sortablelink('name','Nombre')</th>
                    <th scope="col">@sortablelink('email', 'Correo electrónico') </th>
                    <th scope="col">@sortablelink('company', 'Empresa') </th>
                    <th scope="col">@sortablelink('jobTitle', 'Cargo') </th>
                    <th scope="col">@sortablelink('phone', 'telefono') </th>
                    <th scope="col">@sortablelink('patner', 'Socio') </th>
                    <th scope="col">@sortablelink('executive', 'Comisión Directiva') </th>
                    <th scope="col"></th>
                </thead>
                <tbody id="bodyTable">
                    @foreach ($userAbm as $user)
                        <tr>
                            <td><a href="{{ route('userAbm.show', $user->id) }}">{{ $user->name }}</a></td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->company }}</td>
                            <td>{{ isset($user->jobTitle) ? $user->jobTitle : '-' }}</td>
                            <td>{{ $user->phone }}</td>
                            <td>{{ $user->executive == 1 ? 'Si' : 'No' }}</td>
                            <td>{{ $user->patner == 1 ? 'Si' : 'No' }}</td>
                            <td data-excludes="true">
                                @can('news-edit')
                                    <a href="{{ route('userAbm.edit', $user->id) }}" type="button"
                                        class="btn btn-outline-primary  btn-sm  my-1"><i class="fas fa-edit"></i></a>
                                @endcan
                                @can('news-delete')
                                    <form name="form-delete" action="{{ route('userAbm.delete', $user->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-outline-danger btn-sm" disabled><i class="fas fa-trash-alt"></i></button>
                                    </form>
                                @endcan

                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $userAbm->appends(\Request::except('page'))->render() }}
        </div>

    </div>


@endsection
<script src="{{ asset('js/delete.js') }}" defer></script>
