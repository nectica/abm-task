@extends('form.index')
@section('title', 'Actualizar contraseña')
@section('title-form', 'Actualizar contraseña')
@section('route-form', route('userAbm.updatePassword'))
@section('content-form')
    @if ($message = Session::get('success'))
        <div class=" col-6  mt-2 alert alert-success">
            <p>{{ $message }}</p>
        </div>


    @elseif ($token == $userAbm->token)
        @method('PUT')
        <input type="hidden" name="id" value="{{ $id }}">
        <input type="hidden" name="token_user" value="{{$token }}">
        <div class="form-group">
            <label for="name">Nombre</label>
            <input type="text" disabled class="form-control" value="{{ old('name', $userAbm->name) }}">
        </div>
        <div class="form-group">
            <label for="name">email</label>
            <input type="email" class="form-control" disabled value="{{ old('email', $userAbm->email) }}">
        </div>
        <div class="form-group">
            <label for="password">Contraseña</label>
            <input type="password" class="form-control" id="password" name="password" value="{{ old('password') }}">
        </div>
        <div class="form-group">
            <label for="password_confirmation">Confirme contraseña</label>
            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation"
                value="{{ old('password_confirmation') }}">
        </div>
        <button type="submit" class="btn btn-primary">Actualizar el contraseña</button>
    @else
        <div class="row justify-content-center">
            <h4>
                Token no valido
            </h4>
        </div>
    @endif
@endsection
