@extends('layouts.layout')
@section('title', $userAbm->name)
@section('content')
<div class="row  m-3" >
    <div class="col-4 offset-1">
        <h2>{{$userAbm->name}}</h2>
    </div>

</div>
<div class="row justify-content-center mb-5 px-2">
    <div class="col-8">
        <p class="lead">
           Email: {{$userAbm->email}}
        </p>
    </div>
    <div class="col-8">
        <p class="lead">
            Tipo de usuario:
            {{ $userAbm->patner ? 'Es socio' : '' }}
            {{ $userAbm->patner && $userAbm->executive ? ' y ' : '' }}
            {{ $userAbm->executive ? 'Es comision directiva' : '' }}
            {{ !$userAbm->patner && !$userAbm->executive ? 'Ninguno' : '.' }}

        </p>
    </div>
</div>


@endsection

