@extends('form.index')
@section('title', 'Actualizar el Usuario abm')
@section('title-form', 'Actualizar el Usuario abm.')
@section('route-form', route('userAbm.update', $userAbm->id))
@section('content-form')
    @method('PUT')
	<input type="hidden" name="url" value="{{ old('url', url()->previous()) }}" />
    <input type="hidden" name="id" value="{{$userAbm->id}}">
    <div class="form-group">
        <label for="name">Nombre</label>
        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $userAbm->name)  }}">
    </div>
    <div class="form-group">
        <label for="name">email</label>
        <input type="email" class="form-control" id="email" name="email" value="{{ old('email', $userAbm->email)    }}">
    </div>
    <div class="form-group">
        <label for="password">Contraseña</label>
        <input type="password" class="form-control" id="password" name="password" value="{{ old('password') }}">
    </div>
    <div class="form-group">
        <label for="password_confirmation">Confirme contraseña</label>
        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" value="{{ old('password_confirmation') }}">
    </div>
    <div class="form-group">
        <label for="phone">Teléfono</label>
        <input type="text" class="form-control" id="phone" name="phone" value="{{ old('phone', $userAbm->phone) }}">
    </div>
    <div class="form-group">
        <label for="company">Empresa</label>
        <input type="text" class="form-control" id="company" name="company" value="{{ old('company', $userAbm->company) }}">
    </div>
    <div class="form-group">
        <label for="jobTitle">Puesto</label>
        <input type="text" class="form-control" id="jobTitle" name="jobTitle" value="{{ old('jobTitle', $userAbm->jobTitle) }}">
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="checkbox" id="executive" value="executive" name="userType[]"
        {{ ((is_array( old('userType') ) && in_array('executive', old('userType')) ) || (!is_array( old('userType') ) && $userAbm->executive ) )  ? 'checked' : '' }}
            >
        <label class="form-check-label" for="executive">Comisión directiva</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="checkbox" id="patner" value="patner" name="userType[]"
        {{ ( (is_array( old('userType') ) && in_array('patner', old('userType') ) ) || (!is_array( old('userType') ) && $userAbm->patner ) )  ? 'checked' : '' }}>
        <label class="form-check-label" for="patner">Socio</label>
    </div>
    <button type="submit" class="btn btn-primary">Actualizar el Usuario</button>
@endsection


