@extends('form.index')
@section('title', 'Crear usuario')
@section('title-form', 'Crear usuario.')
@section('route-form', route('userAbm.store'))
@section('content-form')
    <div class="form-group">
        <label for="name">Nombre</label>
        <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
    </div>
    <div class="form-group">
        <label for="email">email</label>
        <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
    </div>
    <div class="form-group">
        <label for="password">Contraseña</label>
        <input type="password" class="form-control" id="password" name="password" value="{{ old('password') }}">
    </div>
    <div class="form-group">
        <label for="password_confirmation">Confirme contraseña</label>
        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" value="{{ old('password_confirmation') }}">
    </div>
    <div class="form-group">
        <label for="phone">Teléfono</label>
        <input type="text" class="form-control" id="phone" name="phone" value="{{ old('phone') }}">
    </div>
    <div class="form-group">
        <label for="company">Empresa</label>
        <input type="text" class="form-control" id="company" name="company" value="{{ old('company') }}">
    </div>
    <div class="form-group">
        <label for="jobTitle">Puesto</label>
        <input type="text" class="form-control" id="jobTitle" name="jobTitle" value="{{ old('jobTitle') }}">
    </div>
    <div class="form-group">
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="executive" value="executive" name="userType[]"
                {{ is_array(old('userType')) && in_array('executive', old('userType')) ? 'checked' : '' }}>
            <label class="form-check-label" for="executive">Comisión directiva</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="patner" value="patner" name="userType[]"
                {{ is_array(old('userType')) && in_array('patner', old('userType')) ? 'checked' : '' }}>
            <label class="form-check-label" for="patner">Socio</label>
        </div>
    </div>
    <button type="submit" class="btn btn-success-caem">Crear Usuario abm</button>
@endsection
